﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Security.Principal;

namespace S4D.Database.Services
{
    public class UserPatternConverter : log4net.Layout.Pattern.PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            string id = "";

            try
            {
                WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();
                if (windowsIdentity != null && windowsIdentity.Name != null)
                    id = windowsIdentity.Name;
            }
            catch { }

            writer.Write(id);
        }
    }
}
