﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;
using NConsoler;
using log4net;

namespace S4D.Database.Services
{
    partial class Program : IMessenger
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        public void Write(string message)
        {
            logger.InfoFormat("Consolery message:{0}", message);
            Console.WriteLine(message);
        }

        static int Main(string[] args)
        {
            // Culture inicialization
            Thread.CurrentThread.Name = "Main";
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("cs-CZ");
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            string programVesion = GetProgramVersion();

            Console.WriteLine("System4u S4dbsrvc.exe application version {0}.", programVesion);
            Console.WriteLine("Copyright (c) System4u s.r.o. | www.System4u.cz");
            Console.WriteLine();

            // start
            DateTime start = DateTime.Now;
            logger.InfoFormat("Program version {0} starts {1}.", programVesion, start.ToString("ddd dd.MM.yyyy HH:mm:ss"));

            try
            {
                Environment.ExitCode = 0;
                Consolery.Run(typeof(Program), args, new Program(), Notation.Windows);

                if (Environment.ExitCode == 0)
                    logger.InfoFormat("Successfully done for {0}.", DateTime.Now - start);
            }
            catch (Exception ex)
            {
                logger.Fatal("Program terminated by an error!", ex);
                Environment.ExitCode = 1;
            }

            FlushLogBuffers();
            return Environment.ExitCode;
        }

        internal static String GetProgramVersion()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            if (asm != null)
            {
                AssemblyName asmName = asm.GetName();
                if (asmName != null)
                {
                    Version asmVer = asmName.Version;
                    if (asmVer != null)
                    {
                        DateTime date = new DateTime(2000, 1, 1);
                        date = date.AddDays(asmVer.Build);
                        return string.Format("{0} ({1:yyyy'-'MM'-'dd})", asmVer.ToString(), date);
                    }
                }
            }

            return "[unknown]";
        }

        internal static void FlushLogBuffers()
        {
            log4net.Repository.ILoggerRepository rep = log4net.LogManager.GetRepository();
            foreach (log4net.Appender.IAppender appender in rep.GetAppenders())
            {
                var buffered = appender as log4net.Appender.BufferingAppenderSkeleton;
                if (buffered != null)
                {
                    try
                    {
                        buffered.Flush();
                    }
                    catch { }
                }
            }
        }
    }
}
