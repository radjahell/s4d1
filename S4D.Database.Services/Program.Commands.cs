﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using log4net;
using NConsoler;
using S4D.Database;

namespace S4D.Database.Services
{
    partial class Program
    {
        protected const string connectionStringName = "s4dSrvcMain";

        protected const int supportedDatabaseVersion = 1;

        /// <summary>
        /// Zalozi program do windows eventlogu - program musi bezet v admin modu
        /// </summary>
        [Action(Description = "- Windows EventLog program register.")]
        public static void WinEvnt()
        {
            logger.Fatal("Start Windows EventLog registering.");
        }

        /// <summary>
        /// Zalozi databazi
        /// </summary>
        [Action(Description = "- create database.")]
        public static void NewDb()
        {
            logger.Debug("Start create new database.");

            DataManager.CreateDatabase(connectionStringName);

            logger.Info("Database created.");
        }

        /// <summary>
        /// Vymaze EventLog
        /// </summary>
        [Action(Description = "- clear EventLog database table.")]
        public static void ClrLog()
        {
            logger.Debug("Start clear EventLog");

            String programVersion = GetProgramVersion();

            logger.DebugFormat("Selected database catalog is '{0}'.", DataManager.FindCalalogName(connectionStringName));
            logger.DebugFormat("Current database version is {0}.", DataManager.CheckDatabaseVersion(connectionStringName, programVersion, supportedDatabaseVersion));

            if (DataManager.ClearEventLog(connectionStringName))
                logger.Info("EventLog table is empty.");
        }
    }
}
