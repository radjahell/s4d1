﻿using S4D.Database;
using S4D.Database.Model;
using S4D.Api.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using System.Threading.Tasks;
using S4D.Mailing.Interfaces;
using S4D.Mailing;

namespace S4D.Api.Controllers
{
    public class PriceListController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(PriceListController));

        private S4dDbContext context = new S4dDbContext("s4dApiMain");
        private IMailing mailer;


        public PriceListController()
        {
            mailer = (mailer ?? (mailer = new Mailer()));
        }

        [Route("api/getpricelisttypes")]
        [HttpGet]
        public async Task<IList<TypePriceList>> GetPriceListTypes()
        {
            Task.Run(() => { logger.Debug("api/getpricelisttypes"); });

            var priceListTypes = new List<TypePriceList>();
            var query = context.TypePriceList.Select(x => new { Id = x.Id, Description = x.Description, Type = x.Type });

            foreach (var item in query)
            {
                TypePriceList typePriceList = new TypePriceList
                {
                    Id = item.Id,
                    Description = item.Description,
                    Type = item.Type
                };
                priceListTypes.Add(typePriceList);
            }
            return priceListTypes;
        }

        [Route("api/getdatatypes")]
        [HttpGet]
        public async Task<IList<TypeData>> GetDataTypes()
        {
            Task.Run(() => { logger.Debug("api/getdatatypes"); });
            var query = context.TypeData.Select(x => new { Id = x.Id, Description = x.Description, Type = x.Type, LongDescription = x.LongDescription });

            var dataTypes = new List<TypeData>();

            foreach (var item in query)
            {
                TypeData typeData = new TypeData
                {
                    Id = item.Id,
                    Description = item.Description,
                    Type = item.Type,
                    LongDescription = item.LongDescription
                };
                dataTypes.Add(typeData);
            }

            return dataTypes;
        }

        [Route("api/getbuyers")]
        [HttpGet]
        public async Task<IList<Buyer>> GetBuyers()
        {
            Task.Run(() => { logger.Debug("api/getbuyers"); });
            var query = context.Buyers.Select(x => new { BuyerId = x.BuyerId, Email = x.Email });

            var buyers = new List<Buyer>();

            foreach (var item in query)
            {
                Buyer buyer = new Buyer();
                buyer.BuyerId = item.BuyerId;
                buyer.Email = item.Email;
                buyers.Add(buyer);
            }

            return buyers;

        }

        [Route("api/file/{id}")]
        [HttpGet]
        public async Task<IQueryable<FileSent>> GetFile(int id)
        {
            Task.Run(() => { logger.Debug("api/file"); });

            var sentFilesList = new List<FileSent>();
            var sentFiles = await context.SentFiles.Select(x => new { FileName = x.FileName, Id = x.Id, Message = x.Message, Size = x.Size, Created = x.Created }).Where(x => x.Id == id).ToListAsync();

            foreach (var item in sentFiles)
            {
                FileSent sentFile = new FileSent();
                sentFile.Id = item.Id;
                sentFile.FileName = item.FileName;
                sentFile.Message = item.Message;
                sentFile.Size = item.Size;
                sentFile.Created = item.Created;
                sentFilesList.Add(sentFile);
            }
            return sentFilesList.AsQueryable();
        }

        [Route("api/files")]
        [HttpGet]
        public async Task<IQueryable<FileSent>> GetFiles()
        {
            Task.Run(() => { logger.Debug("api/files"); });

            var sentFilesList = new List<FileSent>();
            var sentFiles = await context.SentFiles.Select(x => new { FileName = x.FileName, Id = x.Id, Message = x.Message, Size = x.Size, Created = x.Created }).ToListAsync();

            foreach (var item in sentFiles)
            {
                FileSent sentFile = new FileSent();
                sentFile.Id = item.Id;
                sentFile.FileName = item.FileName;
                sentFile.Message = item.Message;
                sentFile.Size = item.Size;
                sentFile.Created = item.Created;
                sentFilesList.Add(sentFile);
            }
            return sentFilesList.AsQueryable();
        }

        [Route("api/delete/{Id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> Delete(int Id)
        {
            Task.Run(() => { logger.Debug(String.Concat("api/delete/", Id)); });

            try
            {
                var sentFile = context.SentFiles.Select(x => x).Where(x => x.Id == Id).FirstOrDefault();
                var sentFileResponse = context.SentFiles.Remove(sentFile);
                await context.SaveChangesAsync();

                return Request.CreateResponse(HttpStatusCode.OK, sentFileResponse.Id);
            }
            catch (Exception exception)
            {
                Task.Run(() => { logger.Fatal(String.Concat("api/delete/", Id, " fail!"), exception); });

                return Request.CreateResponse(HttpStatusCode.NotModified, exception);
            }
        }

        [Route("api/savepriceList")]
        [HttpPost]
        public async void SavePriceList([FromBody]PriceList priceList)
        {
            Task.Run(() => { logger.Debug(String.Concat("api/savepriceList/", priceList == null ? "null" : priceList.Id.ToString())); });

            try
            {
                context.PriceLists.Add(priceList);
                await context.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                Task.Run(() => { logger.Fatal(String.Concat("api/savepriceList/", " fail!"), exception); });
            }
        }

        [Route("api/saveFileInfo/{buyerId}/{fileId}/{message}/{subject}")]
        [HttpPost]
        public async void PostFileInfo(int buyerId, int fileId, string message, string subject)
        {
            Task.Run(() => { logger.Debug(String.Concat("api/saveFileInfo/", buyerId, "/", fileId)); });

            var sentFileFromDb = context.SentFiles.Where(x => x.Id == fileId).FirstOrDefault();
            sentFileFromDb.Message = message;
            sentFileFromDb.Subject = subject;

            context.SentFiles.Attach(sentFileFromDb);
            context.Entry(sentFileFromDb).State = EntityState.Modified;
            context.SaveChanges();

            var buyer = context.Buyers.Where(x => x.BuyerId == buyerId).FirstOrDefault();

            //SEND MAIL WHEN FILE UPLOADED
            Task.Run(() => mailer.SendFileUploadedMail(fileId, buyer.Email, subject, message, sentFileFromDb.FileName, sentFileFromDb.Size, sentFileFromDb.Created, message));
        }


        //OBSOLETE - NOT DELETE FOR NOW
        [Route("api/uploadDataInfo/{buyerId}/{fileId}/{subject}/{typeDataId}/{typeListId}/{size}")]
        [HttpPost]
        public async void PostFileInfo(int buyerId, int fileId, string subject, int typeDataId, int typeListId, long size)
        {
            Task.Run(() => { logger.Debug(String.Concat("api/saveFileInfo/", buyerId, "/", fileId)); });

            var sentFileFromDb = context.PriceLists.FirstOrDefault(x => x.Id == fileId);
            if (sentFileFromDb != null)
            {
                sentFileFromDb.Description = subject;

                sentFileFromDb.TypePriceListId = typeListId;
                sentFileFromDb.BuyerId = buyerId;
                sentFileFromDb.TypeDataId = typeDataId;
                sentFileFromDb.Body = "To BE DEVELOPED";


                context.PriceLists.Attach(sentFileFromDb);
                context.Entry(sentFileFromDb).State = EntityState.Modified;
                context.SaveChanges();

                var buyer = context.Buyers.FirstOrDefault(x => x.BuyerId == buyerId);

                ////SEND MAIL WHEN FILE UPLOADED
                Task.Run(() => mailer.SendFileUploadedMail(fileId, buyer.Email, subject, sentFileFromDb.Description, sentFileFromDb.FileName, sentFileFromDb.Size, sentFileFromDb.Created, sentFileFromDb.Description));
            }
        }


        [Route("api/setPriceListStatusToApproved/{id}")]
        [HttpPost]
        public async void SetPriceListStatusToApprove(int id)
        {
            Task.Run(() => { logger.Debug(String.Concat("api/setPriceListStatusToApproved/", id)); });


            var sentFileFromDb = context.PriceLists.FirstOrDefault(x => x.Id == id);
            if (sentFileFromDb != null)
            {
                try
                {
                    sentFileFromDb.Status = "Approved";

                    context.PriceLists.Attach(sentFileFromDb);
                    context.Entry(sentFileFromDb).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Task.Run(() => { logger.Debug(String.Concat("api/setPriceListStatusToApproved/", ex)); });
                }

            }
        }

        [Route("api/setPriceListStatusToDeclined/{id}")]
        [HttpPost]
        public async void SetPriceListStatusToDeclined(int id)
        {
            Task.Run(() => { logger.Debug(String.Concat("api/setPriceListStatusToDeclined/", id)); });


            var sentFileFromDb = context.PriceLists.FirstOrDefault(x => x.Id == id);
            if (sentFileFromDb != null)
            {
                try
                {
                    sentFileFromDb.Status = "Declined";

                    context.PriceLists.Attach(sentFileFromDb);
                    context.Entry(sentFileFromDb).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Task.Run(() => { logger.Debug(String.Concat("api/setPriceListStatusToDeclined/", ex)); });
                }

            }
        }

        [Route("api/setPriceListStatusToUpdated/{id}")]
        [HttpPost]
        public async void SetPriceListStatusToUpdated(int id)
        {
            Task.Run(() => { logger.Debug(String.Concat("api/setPriceListStatusToUpdated/", id)); });


            var sentFileFromDb = context.PriceLists.FirstOrDefault(x => x.Id == id);
            if (sentFileFromDb != null)
            {
                try
                {
                    sentFileFromDb.Status = "Updated";

                    context.PriceLists.Attach(sentFileFromDb);
                    context.Entry(sentFileFromDb).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    Task.Run(() => { logger.Debug(String.Concat("api/setPriceListStatusToUpdated/", ex)); });
                }

            }
        }

        [Route("api/getPricelistById/{Id}")]
        [HttpGet]
        public async Task<PriceList> GetPricelistById(int id)
        {
            var priceList = await context.PriceLists.Where(x => x.Id == id).SingleAsync();
            return priceList;
        }

        [Route("api/getPriceLists")]
        [HttpGet]
        public async Task<IQueryable<PriceList>> getPriceLists()
        {
            var priceLists = new List<PriceList>();
            var query = await context.PriceLists.Select(x => new
            {
                Id = x.Id,
                Created = x.Created,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Body = x.Body,
                Description = x.Description,
                BuyerId = x.BuyerId,
                SellerId = x.SellerId,
                TypePriceListId = x.TypePriceListId,
                TypeDataId = x.TypeDataId,
                FileName = x.FileName,
                Size = x.Size,
                Status = x.Status
            }).ToListAsync();

            foreach (var item in query)
            {
                PriceList priceList = new PriceList();
                priceList.Id = item.Id;
                priceList.Created = item.Created;
                priceList.StartDate = item.StartDate;
                priceList.EndDate = item.EndDate;
                priceList.Body = item.Body;
                priceList.Description = item.Description;
                priceList.BuyerId = item.BuyerId;
                priceList.SellerId = item.SellerId;
                priceList.TypePriceListId = item.TypePriceListId;
                priceList.TypeDataId = item.TypeDataId;
                priceList.FileName = item.FileName;
                priceList.Size = item.Size;
                priceList.Status = item.Status;

                priceLists.Add(priceList);
            }

            return priceLists.AsQueryable();
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
            throw new NotImplementedException();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
            throw new NotImplementedException();
        }
    }
}
