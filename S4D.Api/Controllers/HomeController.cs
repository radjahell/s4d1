﻿using System.Web.Mvc;
using log4net;

namespace S4D.Api.Controllers
{
    public class HomeController : Controller
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(HomeController));

        public HomeController()
        {
          //  this._repository = new Repository<PriceList>();
        }

        public ActionResult Index()
        {
            logger.Debug("Index");

            // var priceList = unitOfWork.SupplierRepository.GetById(1);
           //  var address = unitOfWork.AddressRepository.GetAll();
            ViewBag.Title = "Home Page";
            return View();
        }
    }
}
