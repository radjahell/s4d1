﻿using S4D.Database;
using S4D.Database.Model;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using Tanka.FileSystem.WebApi.FileSystem;
using Tanka.FileSystem.WebApi.FlowJS;
using log4net;
using S4D.Mailing;
using S4D.Mailing.Interfaces;

namespace S4D.Api.Controllers
{
    [RoutePrefix("folders")]
    public class FilesController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(FilesController));
        private IMailing mailer;
        private int fileId;

        private readonly Flow _flow;
        private readonly FileSystem _fileSystem;
        private S4dDbContext priceListContext = new S4dDbContext("s4dApiMain");

        public FilesController()
        {
            _fileSystem = new FileSystem
            {
                GetFilePathFunc = filePath => string.Format(
                    "{0}/{1}",
                    HostingEnvironment.MapPath("~/App_Data").Replace("\\", "/"),
                    filePath)
            };

            _flow = new Flow(_fileSystem); 
            mailer = (mailer ?? (mailer = new Mailer()));
       
    }

        [Route("{*filePath}")]
        public async Task<HttpResponseMessage> GetFile(string filePath)
        {
            logger.Debug(String.Concat("filePath=", filePath));

            if (string.IsNullOrWhiteSpace(filePath))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid path");
            }

            if (!await _fileSystem.ExistsAsync(filePath))
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "File not found");
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StreamContent(await _fileSystem.OpenReadAsync(filePath));
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = filePath.Substring(filePath.LastIndexOf('/') + 1)
            };

            return response;
        }

        [Route("uploads/{folderName}")]
        public async Task<HttpResponseMessage> Get(string folderName)
        {
            logger.Debug(String.Concat("uploads/", folderName));

            //return new HttpResponseMessage(HttpStatusCode.BadRequest);
            var context = CreateContext(folderName);

            return await _flow.HandleRequest(context);
        }

        [Route("uploads/{folderName}")]
        public async Task<HttpResponseMessage> Post(string folderName)
        {
            logger.Debug(String.Concat("uploads/", folderName));

            var context = CreateContext(folderName);
            context.ProcessFileFunc = (flowRequest, flowContext) =>
            {
                var fileName = flowContext.GetFileName(flowRequest);
                var filePath = GetFilePath(flowContext.GetFilePath(flowRequest));

                filePath = Path.Combine(filePath, fileName);
                var data = System.IO.File.ReadAllBytes(filePath);

                //var fileInfo = new FileInfo(file.LocalFileName);

                if (flowRequest.FlowAngularControllerId == "sendFilePartial")
                {
                    FileSent sentFile = new FileSent();
                    sentFile.Created = DateTime.Now;
                    sentFile.FileData = data;
                    sentFile.FileName = fileName;
                    sentFile.Size = data.Length;

                    //SAVE FILE TO DB 
                    try
                    {
                        priceListContext.SentFiles.Add(sentFile);
                        priceListContext.SaveChanges();
                    }
                    catch (Exception ex)
                    { 
                        Task.Run(() => { logger.Debug("send file error", ex); });
                    }
                    
                    //GET ID OF SAVED FILE
                    fileId = sentFile.Id;
                }
                else
                {
                    PriceList priceList = new PriceList();
                    priceList.File = data;
                    priceList.Created = DateTime.Now;
                    priceList.BuyerId = (int)flowRequest.FlowBuyerId;
                    priceList.SellerId = 1;
                    priceList.TypePriceListId = (int)flowRequest.FlowPriceListTypeId;
                    priceList.TypeDataId = (int)flowRequest.FlowDataTypeId;
                    priceList.StartDate = flowRequest.FlowStartDate.Value;
                    priceList.EndDate = flowRequest.FlowEndDate.Value;
                    priceList.Body = "TO BE DEVELOPED";
                    priceList.FileName = fileName;
                    priceList.Size = data.Length;
                    priceList.Status = "Draft";
                    priceList.Description = flowRequest.FlowSubject;
                    //SAVE FILE TO DB 
                    try
                    {
                        priceListContext.PriceLists.Add(priceList);
                        priceListContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Task.Run(() => { logger.Debug("upload data error", ex); });
                    }

                    //GET ID OF SAVED FILE
                    fileId = priceList.Id;
                }  
                
                //DELETE TEMPORARY FILE
                File.Delete(filePath);

                //SEND MAIL WHEN FILE UPLOADED
              //  Task.Run(() => { mailer.SendFileDownloadedMail("info@solution4data.com", "how are you", "read it - jedu...", sentFile.FileName, sentFile.Size ); });

                var response = flowContext.HttpRequest.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(fileId.ToString());

                return response;
            };

            return await _flow.HandleRequest(context);
        }

        [Route("uploads/getFile/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetFile(int id)
        {
            logger.Debug(String.Concat("uploads/getFile/", id));

            var firstOrDefault = priceListContext.SentFiles.FirstOrDefault(x => x.Id == id);
            if (firstOrDefault != null)
            {
                var fileBytes = firstOrDefault.FileData;
                var fileName = firstOrDefault.FileName;

                var filePath = HostingEnvironment.MapPath("~/App_Data").Replace("\\", "/");
                File.WriteAllBytes(filePath + "/" + fileName, fileBytes); 
            }

            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }

        private FlowRequestContext CreateContext(string folderName)
        {
            var flowRequestContext = new FlowRequestContext(Request)
            {
                GetChunkFileNameFunc = parameters => string.Format(
                    "{1}_{0}.chunk",
                    parameters.FlowIdentifier,
                    parameters.FlowChunkNumber.Value.ToString().PadLeft(8, '0')),
                GetChunkPathFunc = parameters => string.Format("{0}/chunks/{1}", folderName, parameters.FlowIdentifier),
                GetFileNameFunc = parameters => parameters.FlowFilename,
                GetFilePathFunc = parameters => folderName,
                GetTempFileNameFunc = filePath => string.Format("file_{0}.tmp", Guid.NewGuid()),
                GetTempPathFunc = () => string.Format("{0}/temp", folderName),
                MaxFileSize = ulong.MaxValue
            };

            return flowRequestContext;
        }

        private static string GetFilePath(string filePath)
        {
            var path = HostingEnvironment.MapPath("~/App_Data");
            return string.IsNullOrEmpty(path) ? filePath : string.Format("{0}/{1}", path.Replace("\\", "/"), filePath);
        }

    }
}