﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using S4D.Database;

namespace S4D.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(WebApiApplication));

        protected void Application_Start()
        {
            // Create unique error id for log4net
            HttpContext.Current.Application["log4netErroId"] = (DateTime.Now.Ticks / 10) % 1000000000L;

            log4net.Config.XmlConfigurator.Configure();

            try
            {
                String programVers = GetProgramVersion();
                String databaseName = DataManager.FindCalalogName("s4dApiMain");

                logger.Info(String.Concat("S4D API ", programVers, " starts with db='", databaseName, "'."));

                int databaseVersion = DataManager.CheckDatabaseVersion("s4dApiMain", programVers, 1);
                logger.Debug(String.Concat("Database version=", databaseVersion));
            }
            catch (Exception ex)
            {
                logger.Fatal("Database inicialization fail!", ex);
                throw;
            }

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            if (HttpContext.Current.Request.HttpMethod != "OPTIONS")
                return;
            HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
            HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
            HttpContext.Current.Response.End();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Increment ErrorId
            object savedErrorId = HttpContext.Current.Application["log4netErroId"];
            if (savedErrorId is long)
                HttpContext.Current.Application["log4netErroId"] = ((long)savedErrorId) + 1L;

            Exception exc = Server.GetLastError();
            logger.Error(String.Concat("Application error=", savedErrorId, ":"), exc);

            // MainWebApp.WriteDatabaseVersionDetailsToLog();
            FlushLogBuffers();

            // Handle HTTP errors
            if (exc.GetType() == typeof(HttpException))
            {
                if (exc.Message.Contains("NoCatch") || exc.Message.Contains("maxUrlLength"))
                    return;

                Server.Transfer("HttpErrorPage");
            }

            Response.Write("<h2>Global Page Error</h2>\n");
            Response.Write("<p>" + exc.Message + "</p>\n");
            Response.Write("Return to the <a href='/home/index'>Default Page</a>\n");

            Server.ClearError();
        }

        protected void Application_End(object sender, EventArgs e)
        {
            FlushLogBuffers();
        }

        public static String GetProgramVersion()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            if (asm != null)
            {
                AssemblyName asmName = asm.GetName();
                if (asmName != null)
                {
                    Version asmVer = asmName.Version;
                    if (asmVer != null)
                    {
                        DateTime date = new DateTime(2000, 1, 1);
                        date = date.AddDays(asmVer.Build);
                        return string.Format("{0} ({1:yyyy'-'MM'-'dd})", asmVer.ToString(), date);
                    }
                }
            }

            return "[unknown]";
        }

        internal static void FlushLogBuffers()
        {
            log4net.Repository.ILoggerRepository rep = log4net.LogManager.GetRepository();
            foreach (log4net.Appender.IAppender appender in rep.GetAppenders())
            {
                var buffered = appender as log4net.Appender.BufferingAppenderSkeleton;
                if (buffered != null)
                {
                    try
                    {
                        buffered.Flush();
                    }
                    catch { }
                }
            }
        }
    }
}
