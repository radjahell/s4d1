﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Threading;
using System.Security.Principal;
using log4net.Core;
using log4net.Layout.Pattern;

/*
 * Priklad nastaveni log4net:
 * 
 *   <layout type="log4net.Layout.PatternLayout">
 *     <param name="ConversionPattern" value="%d [%t] %-5p [User = %HTTPUser] %m%n"/>
 *     <converter>
 *       <name value="HTTPUser" />
 *       <type value="HttpUserPatternConverter" />
 *     </converter>
 *   </layout>
*/

namespace S4D.Api.Infrascructure
{
    /// <summary>
    /// Jmeno uzivatele z HttpContext
    /// </summary>
    public class HttpUserPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, LoggingEvent loggingEvent)
        {
            string name = "";

            try
            {
                HttpContext context = HttpContext.Current;
                if (context != null && context.User != null && context.User.Identity.IsAuthenticated)
                {
                    name = context.User.Identity.Name;
                }
                else
                {
                    WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();
                    if (windowsIdentity != null && windowsIdentity.Name != null)
                    {
                        name = windowsIdentity.Name;
                    }
                }
            }
            catch { }

            writer.Write(name);
        }
    }

    /// <summary>
    /// Id Chyby z WebApp
    /// </summary>
    public class ErrorIdPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(System.IO.TextWriter writer, LoggingEvent loggingEvent)
        {
            string errorId = "";

            try
            {
                if (HttpContext.Current != null)
                {
                    object savedId = HttpContext.Current.Application["log4nerErroId"];
                    if (savedId is long)
                        errorId = ((long)savedId).ToString();
                }
            }
            catch { }

            writer.Write(errorId);
        }
    }
}
