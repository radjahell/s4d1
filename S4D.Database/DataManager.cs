﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using S4D.Database.Model;

namespace S4D.Database
{
    public class DataManager
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(DataManager));

        public static string FindCalalogName(string cnnString)
        {
            ConnectionStringSettings cnnStringSetting = ConfigurationManager.ConnectionStrings[cnnString];
            if (cnnStringSetting == null || String.IsNullOrEmpty(cnnStringSetting.ConnectionString))
                return "<null>";

            String[] findKeys = new string[] {
                "initial catalog=",
                "database=",
                "initial file name=",
                "attachdbfilename="
            };

            String calalogName = "<unknown>";
            String searchIn = cnnStringSetting.ConnectionString;

            foreach (string toFind in findKeys)
            {
                int catalogIndex = searchIn.IndexOf(toFind, StringComparison.InvariantCultureIgnoreCase);
                if (catalogIndex >= 0)
                {
                    int nextSepar = searchIn.IndexOf(';', catalogIndex);
                    if (nextSepar > toFind.Length)
                    {
                        calalogName = searchIn.Substring(catalogIndex + toFind.Length, nextSepar - catalogIndex - toFind.Length);
                        break;
                    }
                }
            }

            return calalogName;
        }

        public static void CreateDatabase(String cnnString)
        {
            using (S4dDbContext db = new S4dDbContext(cnnString))
            {
                logger.Info(String.Concat("S4D.Database creating in '", cnnString, "'."));

                try
                {
                    System.Data.Entity.Database.SetInitializer<S4dDbContext>(new S4dDbContextInitializer());
                    db.EventLogs.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Fatal("CreateDatabase fail!", ex);
                    throw;
                }
            }
        }

        public static int CheckDatabaseVersion(String cnnString, String programVersion, int supportedDbVersion)
        {
            using (S4dDbContext context = new S4dDbContext(cnnString))
            {
                ProcResult result = null;
                try
                {
                    result = context.GetDatabaseVersion(programVersion).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    logger.Fatal("GetDatabaseVersion procedure throws exception:", ex);
                    throw;
                }

                if (result == null || result.Id < supportedDbVersion)
                    throw new Exception(String.Concat(
                        "Unknown or unsupported database versions ", result == null ? "<null>" : result.Id.ToString()));

                return result.Id;
            }
        }

        public static void OnDaemonStart(String cnnString, String version)
        {
            try
            {
                using (S4dDbContext context = new S4dDbContext(cnnString))
                {
                    ProcResult result = context.OnDaemonStart(version).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Fatal("OnDaemonStart procedure throws exception:", ex);
            }
        }

        public static bool ClearEventLog(String cnnString)
        {
            try
            {
                using (S4dDbContext context = new S4dDbContext(cnnString))
                {
                    while (context.EventLogs.Any())
                    {
                        context.EventLogs.Take(100).ToList().ForEach(e => context.EventLogs.Remove(e));
                        context.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Fatal("ClearEventLog fail!", ex);
                return false;
            }
        }
    }
}
