using System;
using System.Collections.Generic;

namespace S4D.Database.Model
{
    public partial class EventLog
    {
        public long Id { get; set; }
        public System.DateTime Created { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public string User { get; set; }
    }
}
