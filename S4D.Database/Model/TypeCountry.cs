﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class TypeCountry : ITyping
    {
        public TypeCountry()
        {
            Buyers = new HashSet<Buyer>();
            Sellers = new HashSet<Seller>();
            Suppliers = new HashSet<Supplier>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Domain { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Buyer> Buyers { get; set; }
        public virtual ICollection<Seller> Sellers { get; set; }
        public virtual ICollection<Supplier> Suppliers { get; set; }
    }
}
