﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class TypeAddress : ITyping
    { 
        public TypeAddress()
        {
            Addresses = new HashSet<Address>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
    }
}
