﻿using System;
using System.Linq;

namespace S4D.Database.Model
{
    public interface ITyping
    {
        int Id { get; set; }
        string Type { get; set; }
        string Description { get; set; }
    }
}
