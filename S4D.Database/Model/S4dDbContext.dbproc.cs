﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;

namespace S4D.Database.Model
{
    public partial class S4dDbContext
    {
        public virtual IEnumerable<ProcResult> OnDaemonStart(string version)
        {
            return this.Database.SqlQuery<ProcResult>(
                String.Concat(S4dDbContext.DbSchema, ".spS4d_OnDaemonStart @version"), new SqlParameter("version", SqlDbType.NVarChar, 1024) { Value = version });
        }

        public virtual IEnumerable<ProcResult> GetDatabaseVersion(string version)
        {
            return this.Database.SqlQuery<ProcResult>(
                String.Concat(S4dDbContext.DbSchema, ".spS4d_GetDatabaseVersion @version"), new SqlParameter("version", SqlDbType.NVarChar, -1) { Value = version });
        }
    }
}
