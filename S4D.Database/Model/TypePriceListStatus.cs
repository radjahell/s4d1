﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class TypePriceListStatus : ITyping
    {
        public TypePriceListStatus()
        {
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
}
