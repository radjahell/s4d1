﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class ProductMapping : EntityTypeConfiguration<Product>
    {
        public ProductMapping()
        {
            ToTable("Product", S4dDbContext.DbSchema);
            HasKey(m => m.ProductId);
            Property(m => m.ProductId).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Identifier).IsRequired().HasColumnName("Ident");
            Property(m => m.TypeProductId).IsRequired().HasColumnName("Type");
            Property(m => m.Ean).IsRequired().HasMaxLength(128).HasColumnName("Ean");
            Property(m => m.InternalCode).IsRequired().HasMaxLength(128).HasColumnName("InternalCode");
            Property(m => m.Description).HasColumnName("Description");

            HasRequired<TypeProduct>(m => m.TypeProduct).WithMany(m => m.Products).HasForeignKey(m => m.TypeProductId).WillCascadeOnDelete(false);
        }
    }
}
