﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class AddressMapping : EntityTypeConfiguration<Address>
    {
        public AddressMapping()
        {
            ToTable("Address", S4dDbContext.DbSchema);
            HasKey(m => m.AddressId);
            Property(m => m.AddressId).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Identifier).IsRequired().HasColumnName("Ident");
            Property(m => m.Street).IsRequired().HasColumnName("Street");
            Property(m => m.City).IsRequired().HasMaxLength(64).HasColumnName("City");
            Property(m => m.PostCode).IsRequired().HasMaxLength(6).HasColumnName("PostCode");
            Property(m => m.IsMain).IsRequired().HasColumnName("Main").HasColumnType("bit");
            Property(m => m.BuyerId).HasColumnName("BuyerId");
            Property(m => m.TypeAddressId).HasColumnName("TypeAddressId");

            HasRequired<Buyer>(m => m.Buyer).WithMany(m => m.Addresses).HasForeignKey(d => d.BuyerId).WillCascadeOnDelete(false);
            HasRequired<TypeAddress>(m => m.TypeAddress).WithMany(m => m.Addresses).HasForeignKey(d => d.TypeAddressId).WillCascadeOnDelete(false);
        }
    }
}