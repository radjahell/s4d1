﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class SellerMapping : EntityTypeConfiguration<Seller>
    {
        public SellerMapping()
        {
            ToTable("Seller", S4dDbContext.DbSchema);
            HasKey(m => m.SellerId);
            Property(m => m.SellerId).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.FirstName).IsRequired().HasMaxLength(100).HasColumnName("Firstname");
            Property(m => m.Surname).IsRequired().HasMaxLength(100).HasColumnName("Surname");
            Property(m => m.Email).IsRequired().HasMaxLength(320).HasColumnName("Email");
            Property(m => m.CountryId).HasColumnName("CountryId");
            Property(m => m.Description).HasColumnName("Description");

            HasRequired<TypeCountry>(m => m.TypeCountry).WithMany(m => m.Sellers).HasForeignKey(m => m.CountryId).WillCascadeOnDelete(false);
        }
    }
}