﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class TypeDataMapping : EntityTypeConfiguration<TypeData>
    {
        public TypeDataMapping()
        {
            ToTable("TypeData", S4dDbContext.DbSchema);
            HasKey(m => m.Id);
            Property(m => m.Id).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Type).IsRequired().HasMaxLength(64).HasColumnName("Type");
            Property(m => m.Description).HasColumnName("Description");
            Property(m => m.LongDescription).HasColumnName("LongDescription");
        }
    }
}