using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class EventLogMapping : EntityTypeConfiguration<EventLog>
    {
        public EventLogMapping()
        {
            ToTable("EventLog", S4dDbContext.DbSchema);
            HasKey(m => m.Id);
            Property(m => m.Id).IsRequired().HasColumnName("Id").HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Created).IsRequired().HasColumnName("Created").HasColumnType("datetime");
            Property(m => m.Thread).HasMaxLength(255).HasColumnName("Thread");
            Property(m => m.Level).IsRequired().HasMaxLength(64).HasColumnName("Level").IsUnicode(false);
            Property(m => m.Logger).IsRequired().HasMaxLength(255).HasColumnName("Logger");
            Property(m => m.Message).HasColumnName("Message");
            Property(m => m.Exception).HasColumnName("Exception");
            Property(m => m.User).HasMaxLength(64).HasColumnName("User");
        }
    }
}
