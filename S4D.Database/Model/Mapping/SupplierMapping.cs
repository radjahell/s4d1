﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class SupplierMapping : EntityTypeConfiguration<Supplier>
    {
        public SupplierMapping()
        {
            ToTable("Supplier", S4dDbContext.DbSchema);
            HasKey(m => m.SupplierId);
            Property(m => m.SupplierId).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.TypeOwnerId).IsRequired().HasColumnName("TypeOwnerId");
            Property(m => m.Name).IsRequired().HasMaxLength(254).HasColumnName("Name");
            Property(m => m.TypeCountryId).IsRequired().HasColumnName("TypeCountryId");
            Property(m => m.Ico).IsRequired().HasColumnName("Ico");
            Property(m => m.Vat).IsRequired().HasColumnName("Vat");
            Property(m => m.CurrencyCode).IsRequired().HasMaxLength(3).HasColumnName("CountryId").IsUnicode(false);

            HasRequired<TypeOwner>(m => m.TypeOwner).WithMany(m => m.Suppliers).HasForeignKey(m => m.TypeOwnerId).WillCascadeOnDelete(false);
            HasRequired<TypeCountry>(m => m.TypeCountry).WithMany(m => m.Suppliers).HasForeignKey(m => m.TypeCountryId).WillCascadeOnDelete(false);
        }
    }
}