﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class PriceListMapping : EntityTypeConfiguration<PriceList>
    {
        public PriceListMapping()
        {
            ToTable("PriceList", S4dDbContext.DbSchema);
            HasKey(m => m.Id);
            Property(m => m.Id).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Created).IsRequired().HasColumnName("Created");
            Property(m => m.StartDate).IsRequired().HasColumnName("StartDate");
            Property(m => m.EndDate).IsRequired().HasColumnName("EndDate");
            Property(m => m.Body).IsRequired().HasColumnType("xml").HasColumnName("Body");
            Property(m => m.Description).HasColumnName("Description");
            Property(m => m.BuyerId).IsRequired().HasColumnName("BuyerId");
            Property(m => m.SellerId).IsRequired().HasColumnName("SellerId");
            Property(m => m.TypePriceListId).IsRequired().HasColumnName("TypeListId");
            Property(m => m.TypeDataId).IsRequired().HasColumnName("TypeDataId");
            Property(m => m.File).HasColumnName("File");
            Property(m => m.FileName).HasColumnName("FileName");
            Property(m => m.Size).HasColumnName("Size");
            Property(m => m.Status).HasColumnName("Status");

            HasRequired<Buyer>(m => m.Buyer).WithMany(m => m.PriceLists).HasForeignKey(d => d.BuyerId).WillCascadeOnDelete(false);
            HasRequired<Seller>(m => m.Seller).WithMany(m => m.PriceLists).HasForeignKey(d => d.SellerId).WillCascadeOnDelete(false);
            HasRequired<TypePriceList>(m => m.TypePriceList).WithMany(m => m.PriceLists).HasForeignKey(d => d.TypePriceListId).WillCascadeOnDelete(false);
            HasRequired<TypeData>(m => m.TypeData).WithMany(m => m.PriceLists).HasForeignKey(d => d.TypeDataId).WillCascadeOnDelete(false);
        }
    }
}
