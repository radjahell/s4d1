﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class BuyerMapping : EntityTypeConfiguration<Buyer>
    {
        public BuyerMapping()
        {
            ToTable("Buyer", S4dDbContext.DbSchema);
            HasKey(m => m.BuyerId);
            Property(m => m.BuyerId).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.FirstName).IsRequired().HasMaxLength(128).HasColumnName("Firstname");
            Property(m => m.Surname).IsRequired().HasMaxLength(128).HasColumnName("Surname");
            Property(m => m.Email).IsRequired().HasMaxLength(320).HasColumnName("Email").IsUnicode(false);
            Property(m => m.CountryId).HasColumnName("CountryId");
            Property(m => m.Description).HasColumnName("Description");

            HasOptional<TypeCountry>(m => m.TypeCountry).WithMany(m => m.Buyers).HasForeignKey(m => m.CountryId).WillCascadeOnDelete(false);
        }
    }
}
