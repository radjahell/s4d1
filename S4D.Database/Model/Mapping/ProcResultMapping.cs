using System;
using System.Data.Entity.ModelConfiguration;

namespace S4D.Database.Model.Mapping
{
    public partial class ProcResultMapping : EntityTypeConfiguration<ProcResult>
    {
        public ProcResultMapping()
        {
            ToTable("ProcResult", S4dDbContext.DbSchema);
            Property(m => m.Id).IsRequired().HasColumnName("Id");
        }
    }
}
