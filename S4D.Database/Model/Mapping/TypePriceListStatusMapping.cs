﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class TypePriceListStatusMapping : EntityTypeConfiguration<TypePriceListStatus>
    {
        public TypePriceListStatusMapping()
        {
            ToTable("TypePriceListStatus", S4dDbContext.DbSchema);
            HasKey(m => m.Id);
            Property(m => m.Id).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Type).IsRequired().HasMaxLength(64).HasColumnName("Status");
            Property(m => m.Description).HasColumnName("Description");
        }
    }
}