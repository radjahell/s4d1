﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class TypeCountryMapping : EntityTypeConfiguration<TypeCountry>
    {
        public TypeCountryMapping()
        {
            ToTable("TypeCountry", S4dDbContext.DbSchema);
            HasKey(m => m.Id);
            Property(m => m.Id).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Type).IsRequired().HasMaxLength(2).HasColumnName("Code").IsUnicode(false);
            Property(m => m.Domain).IsRequired().HasMaxLength(3).HasColumnName("Domain").IsUnicode(false);
            Property(m => m.Description).HasColumnName("Description");
        }
    }
}