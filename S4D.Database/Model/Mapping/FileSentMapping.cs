﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace S4D.Database.Model.Mapping
{
    public partial class FileSentMapping : EntityTypeConfiguration<FileSent>
    {
        public FileSentMapping()
        {
            ToTable("FileSent", S4dDbContext.DbSchema);
            HasKey(m => m.Id);
            Property(m => m.Id).IsRequired().HasColumnName("Id").HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.Identifier).IsRequired().HasColumnName("Ident");
            Property(m => m.Created).IsRequired().HasColumnName("Created").HasColumnType("datetimeoffset");
            Property(m => m.Modified).HasColumnName("Modified").HasColumnType("datetimeoffset");
            Property(m => m.FileName).IsRequired().HasMaxLength(1024).HasColumnName("FileName");
            Property(m => m.Size).HasColumnName("Size").HasColumnType("bigint");
            Property(m => m.FileData).HasColumnName("FileData");
            Property(m => m.Subject).HasColumnName("Subject");
            Property(m => m.Message).HasColumnName("Message");
        }
    }
}
