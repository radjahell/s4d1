﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class FileReceived : IFile
    {
        public FileReceived()
        {
        }

        public int Id { get; set; }
        public Guid Identifier { get; set; }
        public byte[] FileData { get; set; }
        public string FileName { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset? Modified { get; set; }
        public long? Size { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Receiver { get; set; }
    }
}
