﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class PriceList
    {
        public PriceList()
        {
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Body { get; set; }
        public string Description { get; set; }
        public int BuyerId { get; set; }
        public int SellerId { get; set; }
        public int TypePriceListId { get; set; }
        public int TypeDataId { get; set; }
        public byte[] File { get; set; }
        public string Status { get; set; }
        public long? Size { get; set; }

        public string FileName { get; set; }

        public virtual Buyer Buyer { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual TypePriceList TypePriceList { get; set; }
        public virtual TypeData TypeData { get; set; }
    }
}