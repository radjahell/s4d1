﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace S4D.Database.Model
{
    public partial class S4dDbContext : DbContext
    {
        private static SqlProviderServices instance = SqlProviderServices.Instance;

        public static string DbSchema { get; set; }

        public S4dDbContext()
            : base("name=S4d")
        {
            System.Data.Entity.Database.SetInitializer<S4dDbContext>(null);
        }

        public S4dDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            System.Data.Entity.Database.SetInitializer<S4dDbContext>(null);
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<FileSent> SentFiles { get; set; }
        public DbSet<FileReceived> ReceivedFiles { get; set; }
        public DbSet<PriceList> PriceLists { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }

        // ITyping
        public DbSet<TypeAddress> TypeAddress { get; set; }
        public DbSet<TypeCountry> TypeCountry { get; set; }
        public DbSet<TypeData> TypeData { get; set; }
        public DbSet<TypeOwner> TypeOwner { get; set; }
        public DbSet<TypePriceList> TypePriceList { get; set; }
        public DbSet<TypePriceListStatus> TypePriceListStatus { get; set; }
        public DbSet<TypeProduct> TypeProduct { get; set; }

        // Helpers
        public DbSet<EventLog> EventLogs { get; set; }
        public DbSet<ProcResult> ProcResults { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (S4dDbContext.DbSchema == null)
                S4dDbContext.DbSchema = ConfigurationManager.AppSettings["DbSchema"] ?? "dbo";

            modelBuilder.HasDefaultSchema(S4dDbContext.DbSchema);
            modelBuilder.Configurations.Add(new Mapping.AddressMapping());
            modelBuilder.Configurations.Add(new Mapping.BuyerMapping());
            modelBuilder.Configurations.Add(new Mapping.FileSentMapping());
            modelBuilder.Configurations.Add(new Mapping.FileReceivedMapping());
            modelBuilder.Configurations.Add(new Mapping.PriceListMapping());
            modelBuilder.Configurations.Add(new Mapping.ProductMapping());
            modelBuilder.Configurations.Add(new Mapping.SellerMapping());
            modelBuilder.Configurations.Add(new Mapping.SupplierMapping());

            modelBuilder.Configurations.Add(new Mapping.TypeAddressMapping());
            modelBuilder.Configurations.Add(new Mapping.TypeCountryMapping());
            modelBuilder.Configurations.Add(new Mapping.TypeDataMapping());
            modelBuilder.Configurations.Add(new Mapping.TypeOwnerMapping());
            modelBuilder.Configurations.Add(new Mapping.TypePriceListMapping());
            modelBuilder.Configurations.Add(new Mapping.TypePriceListStatusMapping());
            modelBuilder.Configurations.Add(new Mapping.TypeProductMapping());

            modelBuilder.Configurations.Add(new Mapping.EventLogMapping());
            modelBuilder.Configurations.Add(new Mapping.ProcResultMapping());
        }
    }
}