﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class Supplier
    {
        public Supplier()
        {
        }

        public int SupplierId { get; set; }
        public int TypeOwnerId { get; set; }
        public string Name { get; set; }
        public int TypeCountryId { get; set; }
        public int Ico { get; set; }
        public int Vat { get; set; }
        public string CurrencyCode { get; set; }

        public virtual TypeOwner TypeOwner { get; set; }
        public virtual TypeCountry TypeCountry { get; set; }
    }
}
