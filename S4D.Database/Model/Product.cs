﻿using System;
using System.Collections.Generic;

namespace S4D.Database.Model
{
    public class Product
    {
        public Product()
        {
        }

        public int ProductId { get; set; }
        public Guid Identifier { get; set; }
        public string Ean { get; set; }
        public string InternalCode { get; set; }
        public string Description { get; set; }
        public int TypeProductId { get; set; }
        public virtual TypeProduct TypeProduct { get; set; }
    }
}