﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class Address
    {
        public Address()
        {
        }

        public int AddressId { get; set; }
        public Guid Identifier { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public bool IsMain { get; set; }
        public int BuyerId { get; set; }
        public int TypeAddressId { get; set; }

        public virtual Buyer Buyer { get; set; }
        public virtual TypeAddress TypeAddress { get; set; }
    }
}
