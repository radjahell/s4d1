﻿using System;
using System.Collections.Generic;

namespace S4D.Database.Model
{
    public class Buyer : IName
    {
        public Buyer()
        {
            PriceLists = new HashSet<PriceList>();
            Addresses = new HashSet<Address>();
        }

        public int BuyerId { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public int? CountryId { get; set; }
        public virtual TypeCountry TypeCountry { get; set; }

        public virtual ICollection<PriceList> PriceLists { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}