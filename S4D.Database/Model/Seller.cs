﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class Seller : IName
    {
        public Seller()
        {
            PriceLists = new HashSet<PriceList>();
        }

        public int SellerId { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int? CountryId { get; set; }
        public string Description { get; set; }

        public virtual TypeCountry TypeCountry { get; set; }

        public virtual ICollection<PriceList> PriceLists { get; set; }
    }
}
