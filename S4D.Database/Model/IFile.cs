﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4D.Database.Model
{
    public interface IFile
    {
        int Id { get; set; }
        Guid Identifier { get; set; }
        byte[] FileData { get; set; }
        string FileName { get; set; }
        DateTimeOffset Created { get; set; }
        DateTimeOffset? Modified { get; set; }
        long? Size { get; set; }
        string Subject { get; set; }
        string Message { get; set; }
    }
}
