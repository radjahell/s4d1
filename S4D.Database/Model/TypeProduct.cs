﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class TypeProduct : ITyping
    {
        public TypeProduct()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
