﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S4D.Database.Model
{
    public interface IName
    {
        string FirstName { get; set; }
        string Surname { get; set; }
        string Email { get; set; }
    }
}
