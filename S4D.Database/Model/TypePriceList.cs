﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class TypePriceList : ITyping
    {
        public TypePriceList()
        {
            PriceLists = new HashSet<PriceList>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PriceList> PriceLists { get; set; }
    }
}
