﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace S4D.Database.Model
{
    public class TypeOwner : ITyping
    {
        public TypeOwner()
        {
            Suppliers = new HashSet<Supplier>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Supplier> Suppliers { get; set; }
    }
}
