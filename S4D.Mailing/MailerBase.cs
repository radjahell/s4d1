﻿using MailKit.Net.Smtp;
using MimeKit;
using S4D.Mailing.Services;
using System;
using System.IO;

namespace S4D.Mailing
{
    public class MailerBase
    {

        ConfigurationService configurationService;
        ReplaceVariableService replaceVariableService;

        public MailerBase()
        {
            configurationService = (configurationService ?? (configurationService = new ConfigurationService()));
            replaceVariableService = (replaceVariableService ?? (replaceVariableService = new ReplaceVariableService()));
        }

        protected void SendMail(int fileId, string mailTo, string mailSubject, string mailBody, MailType mailType,string fileName, long? fileSize, DateTimeOffset created, string userMessage)
        {
            //return true for server cert - SMTP4Dev
            System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };


            configurationService.EnsurePathExistsOrCreate();
            MimeMessage message = CreateMimeMessage(mailTo, mailSubject);
            var html = CreateMailBodyByMailType(mailType);
            replaceVariableService.FillInVariables(fileId, fileName, fileSize, mailTo, created, userMessage, mailSubject);
            var replacedHtml = replaceVariableService.PrepareVariablesToReplace(html);
            CreateHtmlBody(message, replacedHtml);

            using (var client = new SmtpClient())
            {
                client.Connect(configurationService.SmtpHost, configurationService.SmtpPort);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                //client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                // client.Authenticate("joey", "password"); 
                client.Send(message);
                client.Disconnect(true);
            }
        }

        private static void CreateHtmlBody(MimeMessage message, string html)
        {
            var builder = new BodyBuilder();
            builder.HtmlBody = string.Format(html);
            message.Body = builder.ToMessageBody();
        }

        private string CreateMailBodyByMailType(MailType mailType)
        {

            string templatePath = string.Empty;

            switch (mailType)
            {
                case MailType.FileDownloaded:
                    templatePath = configurationService.TemplatesFileDownloadedPath;
                    break;
                case MailType.FileSent:
                    templatePath = configurationService.TemplatesFileSentPath;
                    break;
                case MailType.FileUploaded:
                    templatePath = configurationService.TemplatesFileUploadedPath;
                    break;
            }

            return File.ReadAllText(templatePath);
        }

        private MimeMessage CreateMimeMessage(string mailTo, string mailSubject)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(configurationService.CompanyMail));
            message.To.Add(new MailboxAddress(mailTo));
            message.Subject = mailSubject;
            return message;
        }
    }
}
