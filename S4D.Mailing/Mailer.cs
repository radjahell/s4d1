﻿ 
using S4D.Mailing.Interfaces; 
using System;

namespace S4D.Mailing
{
    public enum MailType { FileSent, FileUploaded, FileDownloaded }
 
    public class Mailer : MailerBase, IMailing 
    { 
        public void SendFileDownloadedMail(int fileId, string mailTo, string mailSubject, string mailBody, string fileName, long? fileSize, DateTimeOffset created, string userMessage)
        {
            SendMail(fileId, mailTo, mailSubject, mailBody, MailType.FileDownloaded, fileName, fileSize, created, userMessage);
        }

        public void SendFileUploadedMail(int fileId, string mailTo, string mailSubject, string mailBody, string fileName, long? fileSize, DateTimeOffset created, string userMessage)
        { 
            SendMail(fileId, mailTo, mailSubject, mailBody, MailType.FileUploaded, fileName, fileSize, created, userMessage);
        }

        public void SendFileSentMail(int fileId, string mailTo, string mailSubject, string mailBody, string fileName, long? fileSize, DateTimeOffset created, string userMessage)
        { 
            SendMail(fileId, mailTo, mailSubject, mailBody, MailType.FileSent, fileName, fileSize, created, userMessage);
        } 
    }
}
