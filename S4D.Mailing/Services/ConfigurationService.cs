﻿using S4D.Mailing.Interfaces;
using System;
using System.Configuration;
using System.IO;

namespace S4D.Mailing.Services
{ 
    public class ConfigurationService : IConfigurationService
    {
        private string smtpHost;
        private string companyMail;
        private string smtpPort;
        private string downloadLink;
        private string templatesPath;
        private string templatesFileSentPath;
        private string templatesFileUploadePath;
        private string templatesFileDownloadedPath;


        public void EnsurePathExistsOrCreate() {
            if (!Directory.Exists(TemplatesPath))
                Directory.CreateDirectory(TemplatesPath); 
        }

        public string TemplatesFileDownloadedPath
        {
            get { return templatesFileDownloadedPath ?? (templatesFileDownloadedPath =
                     Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "htmlTemplates", "templateFileDownloaded.html")); }
        }

        public string TemplatesFileUploadedPath
        {
            get { return templatesFileUploadePath ?? (templatesFileUploadePath = 
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "htmlTemplates", "templateFileUploaded.html")); }
        }

        public string TemplatesFileSentPath
        {
            get { return templatesFileSentPath ?? (templatesFileSentPath =
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "htmlTemplates", "templateFileSent.html")); }
        } 

        public string TemplatesPath
        {
            get { return templatesPath ?? (templatesPath = 
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "htmlTemplates")); }
        }

        public string SmtpHost
        {
            get { return smtpHost ?? (smtpHost = ConfigurationManager.AppSettings["smtpHost"]); }
        }

        public string CompanyMail
        {
            get { return companyMail ?? (companyMail = ConfigurationManager.AppSettings["companyMail"]); }
        }

        public int SmtpPort
        {
            get { return int.Parse(smtpPort ?? (smtpPort = ConfigurationManager.AppSettings["smtpPort"])); }
        }

        public string DownloadLink
        {
            get { return  downloadLink ?? (downloadLink = ConfigurationManager.AppSettings["downloadLink"] ); }
        } 
    }
}
