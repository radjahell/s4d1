﻿
using System;
using System.Collections.Generic;

namespace S4D.Mailing.Services
{
    public static class StringExtender
    {
        public static string MultipleReplace(this string text, Dictionary<string, string> replacements)
        {
            string retVal = text;
            foreach (string textToReplace in replacements.Keys)
            {
                retVal = retVal.Replace(textToReplace, replacements[textToReplace]);
            }
            return retVal;  
        }
    }

    public class ReplaceVariableService
    {
        ConfigurationService configurationService;

        public ReplaceVariableService()
        {
            configurationService = (configurationService ?? (configurationService = new ConfigurationService()));
        }

        Dictionary<string, string> stringsToReplace = new Dictionary<string, string>(); 

        public void FillInVariables(int fileId, string fileName, long? fileSize, string mailTo, DateTimeOffset created, string userMessage, string mailSubject) {
            stringsToReplace.Add("%%emlWho%%", mailTo);
            stringsToReplace.Add("%%emlSubject%%", mailSubject);
            stringsToReplace.Add("%%emlFileName%%", fileName);
            stringsToReplace.Add("%%emlFileSize%%", fileSize.ToString());
            stringsToReplace.Add("%%emlFileCreated%%", created.ToString());
            stringsToReplace.Add("%%emlFileDeletionScheduled%%", "soon");
            stringsToReplace.Add("%%emlFilePwdProtected%%", "sure");
            stringsToReplace.Add("%%emlDownloadLink%%", "link");
            stringsToReplace.Add("%%emlMessage%%", userMessage);
            stringsToReplace.Add("%%emlFileNameDownloadLink%%", string.Format("{0}/{1}", configurationService.DownloadLink, fileId));   
        } 

        public string PrepareVariablesToReplace(string htmlBody)
        { 
            return htmlBody.MultipleReplace(stringsToReplace);
        }
    }
}
