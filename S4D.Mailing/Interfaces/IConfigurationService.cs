﻿
namespace S4D.Mailing.Interfaces
{
    public interface IConfigurationService
    {
        string TemplatesPath { get; }
        string SmtpHost { get; }
        string CompanyMail { get; }
        int SmtpPort { get; }
        string TemplatesFileDownloadedPath { get; }
        string TemplatesFileUploadedPath { get; }
        string TemplatesFileSentPath { get; }
        void EnsurePathExistsOrCreate();

    }
}
