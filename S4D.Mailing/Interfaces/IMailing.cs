﻿using System;
 
namespace S4D.Mailing.Interfaces
{
    public interface IMailing
    {
        void SendFileDownloadedMail(int fileId, string mailTo, string mailSubject, string mailBody, string fileName, long? fileSize, DateTimeOffset created, string userMessage);
        void SendFileUploadedMail(int fileId, string mailTo, string mailSubject, string mailBody, string fileName, long? fileSize, DateTimeOffset created, string userMessage);
        void SendFileSentMail(int fileId, string mailTo, string mailSubject, string mailBody, string fileName, long? fileSize, DateTimeOffset created, string userMessage);
    }
}
