﻿/*
Post-Deployment Script
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.
 Use SQLCMD syntax to include a file in the post-deployment script.
 Example:      :r .\myfile.sql
 Use SQLCMD syntax to reference a variable in the post-deployment script.
 Example:      :setvar TableName MyTable
               SELECT * FROM [$(TableName)]
--------------------------------------------------------------------------------------
*/

--> sem novinky <--

-- 2016/11/09
IF NOT EXISTS (SELECT TOP 1 [ID] FROM [dbo].[TypeAddress]) BEGIN
  PRINT 'Inserting [dbo].[TypeAddress] rows.';
  INSERT INTO [dbo].[TypeAddress] ([Type], [Description])
    SELECT N'Corespondent', N'xx';
END
GO

IF NOT EXISTS (SELECT TOP 1 [ID] FROM [dbo].[TypeCountry]) BEGIN
  PRINT 'Inserting [dbo].[TypeCountry] rows.';
  INSERT INTO [dbo].[TypeCountry] ([Code], [Domain], [Description])
    SELECT 'AL', '.al', N'Albania' UNION ALL
    SELECT 'AD', '.ad', N'Andorra' UNION ALL
    SELECT 'AT', '.at', N'Austria' UNION ALL
    SELECT 'BY', '.by', N'Belarus' UNION ALL
    SELECT 'BE', '.be', N'Belgium' UNION ALL
    SELECT 'BA', '.ba', N'Bosnia and Herzegovina' UNION ALL
    SELECT 'BG', '.bg', N'Bulgaria' UNION ALL
    SELECT 'HR', '.hr', N'Croatia' UNION ALL
    SELECT 'CY', '.cy', N'Cyprus' UNION ALL
    SELECT 'CZ', '.cz', N'Czech Republic' UNION ALL
    SELECT 'DK', '.dk', N'Denmark' UNION ALL
    SELECT 'EE', '.ee', N'Estonia' UNION ALL
    SELECT 'FO', '.fo', N'Faroe Islands' UNION ALL
    SELECT 'FI', '.fi', N'Finland' UNION ALL
    SELECT 'FR', '.fr', N'France' UNION ALL
    SELECT 'DE', '.de', N'Germany' UNION ALL
    SELECT 'GI', '.gi', N'Gibraltar' UNION ALL
    SELECT 'GR', '.gr', N'Greece' UNION ALL
    SELECT 'HU', '.hu', N'Hungary' UNION ALL
    SELECT 'IS', '.is', N'Iceland' UNION ALL
    SELECT 'IE', '.ie', N'Ireland' UNION ALL
    SELECT 'IM', '.im', N'Isle of Man' UNION ALL
    SELECT 'IT', '.it', N'Italy' UNION ALL
    SELECT 'LV', '.lv', N'Latvia' UNION ALL
    SELECT 'LI', '.li', N'Liechtenstein' UNION ALL
    SELECT 'LT', '.lt', N'Lithuania' UNION ALL
    SELECT 'LU', '.lu', N'Luxembourg' UNION ALL
    SELECT 'MK', '.mk', N'Macedonia' UNION ALL
    SELECT 'MT', '.mt', N'Malta' UNION ALL
    SELECT 'MD', '.md', N'Moldova' UNION ALL
    SELECT 'MC', '.mc', N'Monaco' UNION ALL
    SELECT 'ME', '.me', N'Montenegro' UNION ALL
    SELECT 'NL', '.nl', N'Netherlands' UNION ALL
    SELECT 'NO', '.no', N'Norway' UNION ALL
    SELECT 'PL', '.pl', N'Poland' UNION ALL
    SELECT 'PT', '.pt', N'Portugal' UNION ALL
    SELECT 'RO', '.ro', N'Romania' UNION ALL
    SELECT 'RU', '.ru', N'Russia' UNION ALL
    SELECT 'SM', '.sm', N'San Marino' UNION ALL
    SELECT 'RS', '.rs', N'Serbia' UNION ALL
    SELECT 'SK', '.sk', N'Slovakia' UNION ALL
    SELECT 'SI', '.si', N'Slovenia' UNION ALL
    SELECT 'ES', '.es', N'Spain' UNION ALL
    SELECT 'SE', '.se', N'Sweden' UNION ALL
    SELECT 'CH', '.ch', N'Switzerland' UNION ALL
    SELECT 'UA', '.ua', N'Ukraine' UNION ALL
    SELECT 'GB', '.uk', N'United Kingdom' UNION ALL
    SELECT 'VA', '.va', N'Vatican city' UNION ALL
    SELECT 'RS', '.rs', N'Yugoslavia';
END
GO

IF NOT EXISTS (SELECT TOP 1 [ID] FROM [dbo].[TypeData]) BEGIN
  PRINT 'Inserting [dbo].[TypeData] rows.';
  INSERT INTO [dbo].[TypeData] ([Type])
    SELECT N'Listing Product' UNION ALL
    SELECT N'Listing Supplier' UNION ALL
    SELECT N'Price List';
END
GO

IF NOT EXISTS (SELECT TOP 1 [Id] FROM [dbo].[TypePriceList]) BEGIN
  PRINT 'Inserting [dbo].[TypePriceList] rows.';
  INSERT INTO [dbo].[TypePriceList] ([Type])
    SELECT N'Promo' UNION ALL
    SELECT N'Listing' UNION ALL
    SELECT N'Standart' UNION ALL
    SELECT N'Special';
END
GO

IF NOT EXISTS (SELECT TOP 1 [ID] FROM [dbo].[Buyer]) BEGIN
  PRINT 'Inserting [dbo].[Buyer] rows.';
  INSERT INTO [dbo].[Buyer] ([Email], [FirstName], [Surname], [Description], [CountryId])
    SELECT N'email@xx.com',  N'JAN',    N'Penkala', N'XX', 10 UNION ALL
    SELECT N'jossif@xx.com', N'Jossif', N'Mifsud',  N'XX', 1 UNION ALL
    SELECT N'kubus@xx.com',  N'Michal', N'Kubus',   N'XX', 11;
END
GO

IF NOT EXISTS (SELECT TOP 1 [ID] FROM [dbo].[Seller]) BEGIN
  PRINT 'Inserting [dbo].[Seller] rows.';
  INSERT INTO [dbo].[Seller] ([FirstName], [Surname], [Email], [Description], [CountryId])
    SELECT N'Petr', N'Kubica', N'kubica@xx.com', N'XX', 10;
END
GO

IF NOT EXISTS (SELECT TOP 1 [ID] FROM [dbo].[Address]) BEGIN
  PRINT 'Inserting [dbo].[Adddress] rows.';
  INSERT INTO [dbo].[Address] ([Ident], [City], [Main], [PostCode], [Street], [BuyerId], [TypeAddressId])
    SELECT NEWID(), N'Moscow', 0, N'72525', N'vYCHODNI', 1, 1 UNION ALL
    SELECT NEWID(), N'Nová Ves u Nového Města na Moravě', 0, N'111 50', N'Nábřeží Svazu protifašistických bojovníků 15', 1, 1;
END
GO

IF NOT EXISTS (SELECT TOP 1 [ID] FROM [dbo].[PriceList]) BEGIN
  PRINT 'Inserting [dbo].[PriceList] rows.';
  INSERT INTO [dbo].[PriceList] ([Body],[Created],[StartDate],[EndDate],[Description],[BuyerId],[SellerId],[TypeDataId],[TypeListId],[FileName],[Size],[Status]) VALUES (
    N'My',
    SYSDATETIMEOFFSET(),
    SYSDATETIMEOFFSET(),
    SYSDATETIMEOFFSET(),
    N'MyPRiceList',
    (SELECT [Id] FROM [dbo].[Buyer] WHERE [Email] = N'email@xx.com'),
    (SELECT [Id] FROM [dbo].[Seller] WHERE [Surname] = N'Kubica'),
    (SELECT [Id] FROM [dbo].[TypeData] WHERE [Type] = N'Listing Product'),
    (SELECT [Id] FROM [dbo].[TypePriceList] WHERE [Type] = N'Promo'),
	N'TESTFILE',
	101,
	 N'Approved' 
	);
END
GO
