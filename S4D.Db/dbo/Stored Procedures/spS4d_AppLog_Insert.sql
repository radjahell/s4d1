﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/03
-- Description: Podle predanych parametru vlozi 
--              zaznam do tabulky [AppLog] a 
--              vrati LogID zalozeneho zaznamu v @LogID
-- =============================================
CREATE PROCEDURE [dbo].[spAppLog_Insert]
    @log_date datetime,
    @thread nvarchar(255),
    @log_level nvarchar(64),
    @logger nvarchar(255),
    @message nvarchar(MAX),
    @exception nvarchar(MAX),
    @user nvarchar(64) = null,
    @app nvarchar(MAX) = null
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @LogID BIGINT;

  INSERT INTO [dbo].[EventLog] ([Created], [Thread], [Level], [Logger], [Message], [Exception], [User])
    VALUES (@log_date, @app + ':' + @thread, @log_level, @logger, @message, @exception, @user)

  SET @LogID = SCOPE_IDENTITY();

  SELECT @LogID AS [Id];
  RETURN CAST(@LogID AS INT);
END

GO
GRANT EXECUTE ON OBJECT::[dbo].[spAppLog_Insert] TO [S4dLogger] AS [dbo];
