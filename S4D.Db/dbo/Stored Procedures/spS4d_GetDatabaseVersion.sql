﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Aktualni verze databaze (DDL)
-- =============================================
CREATE PROCEDURE [dbo].[spS4d_GetDatabaseVersion]
  @version nvarchar(max) = null
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @verze nvarchar(max);
  DECLARE @script nvarchar(max);
  DECLARE @result int;

  SET @verze = LTRIM(RTRIM(REPLACE(SUBSTRING(  N'$LastChangedRevision: 1684 $'  , 22, 255), '$', ' ')));
  SET @script = LTRIM(RTRIM(REPLACE(SUBSTRING(  N'$LastChangedDate: 2016-10-13 18:50:13 +0200 (čt, 13 X 2016) $', 18, 255), '$', ' ')));

  SET @result = CAST(@verze AS int);

  -- !!! NEMENIT !!!
  SELECT @result AS Id
  RETURN @result
END
-- eof
GO
