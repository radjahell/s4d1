﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Deamon byl spusten
-- =============================================
CREATE PROCEDURE [dbo].[spS4d_OnDaemonStart]
  @version nvarchar(1024) = null
AS
BEGIN
  SET NOCOUNT ON;

  -- nechame jen poslednich 500 zprav
  DELETE FROM [dbo].[EventLog] WHERE [Id] NOT IN (SELECT TOP 500 [Id] FROM [dbo].[EventLog] ORDER BY [Id] DESC);

  -- !!! NEMENIT !!!
  SELECT 1 AS Id
  RETURN 1
END
-- eof
GO
