﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Odeslane souboury
-- =============================================
CREATE TABLE [dbo].[FileSent]
(
    [Id]       INT                IDENTITY (1, 1) NOT NULL,
    [Ident]    UNIQUEIDENTIFIER   NOT NULL,
    [FileData] VARBINARY (MAX)    NULL,
    [FileName] NVARCHAR (1024)    NOT NULL,
    [Created]  DATETIMEOFFSET (7) NOT NULL,
    [Modified] DATETIMEOFFSET (7) NULL,
    [Size]     BIGINT             NULL,
    [Subject]  NVARCHAR (MAX)     NULL,
    [Message]  NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_dbo.FileSent] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
