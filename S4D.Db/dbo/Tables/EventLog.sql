﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: EventLog log4net logging table
-- =============================================
CREATE TABLE [dbo].[EventLog]
(
    [Id]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Created]   DATETIME       NOT NULL,
    [Thread]    NVARCHAR (255) NULL,
    [Level]     VARCHAR (64)   NOT NULL,
    [Logger]    NVARCHAR (255) NOT NULL,
    [Message]   NVARCHAR (MAX) NULL,
    [Exception] NVARCHAR (MAX) NULL,
    [User]      NVARCHAR (64)  NULL,
    CONSTRAINT [PK_dbo.EventLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
