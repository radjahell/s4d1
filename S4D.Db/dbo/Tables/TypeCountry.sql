﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Ciselnik statu EU
-- =============================================
CREATE TABLE [dbo].[TypeCountry]
(
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (2)    NOT NULL,
    [Domain]      VARCHAR (3)    NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.TypeCountry] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
