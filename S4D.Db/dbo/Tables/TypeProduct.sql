﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Ciselnik vyrobku
-- =============================================
CREATE TABLE [dbo].[TypeProduct]
(
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Type]        NVARCHAR (64)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.TypeProduct] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
