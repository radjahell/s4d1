﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Ciselnik typu ceniku
-- =============================================
CREATE TABLE [dbo].[TypePriceList] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Type]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.TypePriceList] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
