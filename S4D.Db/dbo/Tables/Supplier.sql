﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Dodavatel
-- =============================================
CREATE TABLE [dbo].[Supplier]
(
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [TypeOwnerId]   INT            NOT NULL,
    [Name]          NVARCHAR (254) NOT NULL,
    [TypeCountryId] INT            NOT NULL,
    [Ico]           INT            NOT NULL,
    [Vat]           INT            NOT NULL,
    [CountryId]     VARCHAR (3)    NOT NULL,
    CONSTRAINT [PK_dbo.Supplier] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Supplier_dbo.TypeCountry_TypeCountryId] FOREIGN KEY ([TypeCountryId]) REFERENCES [dbo].[TypeCountry] ([Id]),
    CONSTRAINT [FK_dbo.Supplier_dbo.TypeOwner_TypeOwnerId] FOREIGN KEY ([TypeOwnerId]) REFERENCES [dbo].[TypeOwner] ([Id])
);
GO

CREATE NONCLUSTERED INDEX [IX_TypeOwnerId] ON [dbo].[Supplier]([TypeOwnerId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_TypeCountryId] ON [dbo].[Supplier]([TypeCountryId] ASC);
GO
