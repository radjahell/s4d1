﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Ciselnik typu adres
-- =============================================
CREATE TABLE [dbo].[TypeAddress]
(
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Type]        NVARCHAR (64)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.TypeAddress] PRIMARY KEY CLUSTERED ([Id] ASC),
);
GO
