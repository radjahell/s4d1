﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Ciselnik majitelu
-- =============================================
CREATE TABLE [dbo].[TypeOwner]
(
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Type]        NVARCHAR (64)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.TypeOwner] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
