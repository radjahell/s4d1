-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Produkt zbozi
-- =============================================
CREATE TABLE [dbo].[Product]
(
    [Id]           INT              IDENTITY (1, 1) NOT NULL,
    [Ident]        UNIQUEIDENTIFIER NOT NULL,
    [Ean]          NVARCHAR (128)   NOT NULL,
    [InternalCode] NVARCHAR (128)   NOT NULL,
    [Description]  NVARCHAR (MAX)   NULL,
    [Type]         INT              NOT NULL,
    CONSTRAINT [PK_dbo.Product] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Product_dbo.TypeProduct_Type] FOREIGN KEY ([Type]) REFERENCES [dbo].[TypeProduct] ([Id])
);
GO

CREATE NONCLUSTERED INDEX [IX_Type] ON [dbo].[Product]([Type] ASC);
GO
