﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Kupujici
-- =============================================
CREATE TABLE [dbo].[Buyer]
(
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Firstname]   NVARCHAR (128) NOT NULL,
    [Surname]     NVARCHAR (128) NOT NULL,
    [Email]       VARCHAR (320)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    [CountryId]   INT            NULL,
    CONSTRAINT [PK_dbo.Buyer] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Buyer_dbo.TypeCountry_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[TypeCountry] ([Id])
);
GO

CREATE NONCLUSTERED INDEX [IX_CountryId] ON [dbo].[Buyer]([CountryId] ASC);
GO
