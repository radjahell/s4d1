-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Cenik
-- =============================================
CREATE TABLE [dbo].[PriceList]
(
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Created]     DATETIME        NOT NULL,
    [StartDate]   DATETIME        NOT NULL,
    [EndDate]     DATETIME        NOT NULL,
    [Body]        XML             NOT NULL,
    [Description] NVARCHAR (MAX)  NULL,
    [BuyerId]     INT             NOT NULL,
    [SellerId]    INT             NOT NULL,
    [TypeListId]  INT             NOT NULL,
    [TypeDataId]  INT             NOT NULL,
    [File]        VARBINARY (MAX) NULL,
    [FileName] NVARCHAR(1024) NOT NULL, 
    [Size] BIGINT NULL, 
    [Status] NVARCHAR(100) NOT NULL, 
    CONSTRAINT [PK_dbo.PriceList] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.PriceList_dbo.Buyer_BuyerId] FOREIGN KEY ([BuyerId]) REFERENCES [dbo].[Buyer] ([Id]),
    CONSTRAINT [FK_dbo.PriceList_dbo.Seller_SellerId] FOREIGN KEY ([SellerId]) REFERENCES [dbo].[Seller] ([Id]),
    CONSTRAINT [FK_dbo.PriceList_dbo.TypeData_TypeDataId] FOREIGN KEY ([TypeDataId]) REFERENCES [dbo].[TypeData] ([Id]),
    CONSTRAINT [FK_dbo.PriceList_dbo.TypePriceList_TypeListId] FOREIGN KEY ([TypeListId]) REFERENCES [dbo].[TypePriceList] ([Id])
);
GO

CREATE NONCLUSTERED INDEX [IX_BuyerId] ON [dbo].[PriceList]([BuyerId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_SellerId] ON [dbo].[PriceList]([SellerId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_TypeDataId] ON [dbo].[PriceList]([TypeDataId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_TypeListId] ON [dbo].[PriceList]([TypeListId] ASC);
GO
