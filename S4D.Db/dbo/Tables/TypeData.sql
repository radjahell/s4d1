﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Ciselnik typu dat
-- =============================================
CREATE TABLE [dbo].[TypeData]
(
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Type]            NVARCHAR (64)  NOT NULL,
    [Description]     NVARCHAR (MAX) NULL,
    [LongDescription] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.TypeData] PRIMARY KEY CLUSTERED ([Id] ASC)
);
