﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Prodavajici
-- =============================================
CREATE TABLE [dbo].[Seller]
(
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Firstname]   NVARCHAR (100) NOT NULL,
    [Surname]     NVARCHAR (100) NOT NULL,
    [Email]       NVARCHAR (320) NOT NULL,
    [CountryId]   INT            NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Seller] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Seller_dbo.TypeCountry_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[TypeCountry] ([Id])
);
GO

CREATE NONCLUSTERED INDEX [IX_CountryId] ON [dbo].[Seller]([CountryId] ASC);
GO
