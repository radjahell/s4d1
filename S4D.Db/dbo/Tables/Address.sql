﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Adresy
-- =============================================
CREATE TABLE [dbo].[Address]
(
    [Id]            INT              IDENTITY (1, 1) NOT NULL,
    [Ident]         UNIQUEIDENTIFIER NOT NULL,
    [Street]        NVARCHAR (MAX)   NOT NULL,
    [City]          NVARCHAR (64)    NOT NULL,
    [PostCode]      NVARCHAR (6)     NOT NULL,
    [Main]          BIT              NOT NULL,
    [BuyerId]       INT              NOT NULL,
    [TypeAddressId] INT              NOT NULL,
    CONSTRAINT [PK_dbo.Address] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.Address_dbo.Buyer_BuyerId] FOREIGN KEY ([BuyerId]) REFERENCES [dbo].[Buyer] ([Id]),
    CONSTRAINT [FK_dbo.Address_dbo.TypeAddress_TypeAddressId] FOREIGN KEY ([TypeAddressId]) REFERENCES [dbo].[TypeAddress] ([Id])
);
GO

CREATE NONCLUSTERED INDEX [IX_BuyerId] ON [dbo].[Address]([BuyerId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_TypeAddressId] ON [dbo].[Address]([TypeAddressId] ASC);
GO
