﻿-- =============================================
-- Author:      RCH
-- Create date: 2016/12/02
-- Description: Ciselnik stavu ceniku
-- =============================================
CREATE TABLE [dbo].[TypePriceListStatus]
(
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Status]      NVARCHAR (64)  NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.TypePriceListStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
