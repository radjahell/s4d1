﻿angular.module('jarapp')
 .service('repository', ['$http', 'appSettings', function ($http, appSettings) {

     this.getPriceListById = function (id) {
         var priceList = $http.get(appSettings.serverPath + '/api/getPricelistById/' + id);
         return priceList;
     };

     this.getPriceLists = function () {
         return $http.get(appSettings.serverPath + '/api/getPriceLists');
     };

     this.getSupplier = function (id) {
         var supplier = $http.get(appSettings.serverPath + '/api/supplier', { params: { "id": id } });
         return supplier;
     };

     this.getSuppliers = function () {
         return $http.get(appSettings.serverPath + '/api/supplier');
     };

     this.getpricelisttypes = function () {
         return $http.get(appSettings.serverPath + '/api/getpricelisttypes');
     };

     this.getdatatypes = function () {
         return $http.get(appSettings.serverPath + '/api/getdatatypes');
     };

     this.getbuyers = function () {
         return $http.get(appSettings.serverPath + '/api/getbuyers');
     };

     var config = {
         headers: {
             'Content-Type': 'application/json'
             //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
         }
     };


     this.setPriceListStatusToDeclined = function (id) {
         var postPath = appSettings.serverPath + '/api/setPriceListStatusToDeclined/' + id;
         return $http.post(postPath);
     }


     this.setPriceListStatusToUpdated = function (id) {
         var postPath = appSettings.serverPath + '/api/setPriceListStatusToUpdated/' + id;
         return $http.post(postPath);
     }

     this.setPriceListStatusToApproved = function (id) {
         var postPath = appSettings.serverPath + '/api/setPriceListStatusToApproved/' + id;
         return $http.post(postPath);
     }

     this.uploadData = function (uploadData) {
         var postPath = appSettings.serverPath + '/api/uploadDataInfo/' + uploadData.buyerId + '/' + uploadData.id + '/' + uploadData.subject + '/' + uploadData.typeDataId + '/' + uploadData.typeListId + '/' + uploadData.size;
         return $http.post(postPath);
     };

     this.sendFile = function (sentFile) {

         var sellerId = 1;
         var buyerId = sentFile.buyerId;
         var fileId = sentFile;
         var postPath = appSettings.serverPath + '/api/saveFileInfo/' + buyerId + '/' + fileId.id + '/' + sentFile.message + '/' + sentFile.subject;

         return $http.post(postPath);


         //    $http({  
         //        url: appSettings.serverPath + '/api/savepriceList',  
         //        dataType: 'json',  
         //        method: 'POST',  
         //        data: priceListViewModel,  
         //        headers: {  
         //            "Content-Type": "application/json"  
         //        }  
         //    }).success(function (response) {  
         //        $scope.value = response;  
         //    })  
         //.error(function (error) {  
         //    alert(error);  
         //});  
         //}  

         //var priceListViewModel = { PersonId: 1, Name: "James" }

         //return $http({
         //    url: appSettings.serverPath + '/api/savepriceList',
         //    method: "POST",
         //    data: priceListViewModel,
         //    //contentType: "application/json",
         //    headers: {
         //        //'Content-Type': 'application/json'
         //        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
         //    }
         //});
     };

 }]);



