﻿'use strict';

angular.module('jarapp')
           .factory('translateService',
    ["$translate", function ($translate) {

        var changeLanguage = function (langKey) { 
            $translate.use(langKey);
        };

        return {
            changeLanguage: changeLanguage
        }; 
    }]);