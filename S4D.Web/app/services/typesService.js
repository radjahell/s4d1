﻿'use strict';

angular.module('jarapp').service('typesService', ['appSettings', '$translate', '$rootScope', 'repository', function (appSettings, $translate, $rootScope, repository) {
  
    this.getDataTypes = function () {
        return [
              {
                  name: 'PRICELIST',
                  value: 2
              },
              {
                  name: 'Listing Product',
                  value: 1
              },
              {
                  name: 'Listing Supplier ',
                  value: 0
              }
        ];
    };


        this.getPriceListTypes = function () { 
            repository.getpricelisttypes()
            .then(function (files) { 
                console.log('Files DATA');
                console.log(files.data );
                return files.data; 
            }).catch(function(response) {
                alertService.add('error', 'Error while getting pricelist types  Error: ' + response.statusText);
            }); 
        }; 


    //    return [
    //          {
    //              name: 'Standart',
    //              value: 0
    //          },
    //          {
    //              name: 'Promo',
    //              value: 1
    //          },
    //            {
    //                name: 'Listing',
    //                value: 2
    //            },
    //          {
    //              name: 'Special',
    //              value: 3
    //          }
    //    ];
    //};

    this.getDataUploadReceivers = function () {
        return [
            {
                name: 'Josef.G@tesco.se',
                value: 1
            },
            {
                name: 'Jan.P@gmail.com',
                value: 2
            },
            {
                name: 'Miklosz.G@tesco.ru',
                value: 3
            },
            {
                name: 'Contact',
                value: 4
            }];
    };

    this.getReceivers = function () {
        return [
               {
                   name: 'Josef.G@tesco.se',
                   value: 1
               },
               {
                   name: 'Jan.P@gmail.com',
                   value: 2
               },
               {
                   name: 'Miklosz.G@tesco.ru',
                   value: 3
               },
               {
                   name: 'Contact',
                   value: 4
               }];
    };

}]);


