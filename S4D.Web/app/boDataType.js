﻿/*
 * Angular custom filters and directive for recurrent Back Office data types
 */
(function () {
    'use strict';

    angular.module('boDataType', [])
    .filter('boDateTime', boDateTime)
    .filter('boDate', boDate)
    .directive('boBoolean', boBoolean);

    boDateTime.$inject = ['$filter'];
    function boDateTime($filter) {
        // Filter that display the date and the time
        // Ex: {{article.StartDate | boDateTime}}
        var angularDateFilter = $filter('date');
        return function (theDate) {
            return angularDateFilter(theDate, 'dd/MM/yyyy HH:mm:ss'); // Format : 30/11/2013 23:25:00
        };
    }

    boDate.$inject = ['$filter'];
    function boDate($filter) {
        // Filter that display the date
        // Ex: {{article.StartDate | boDate}}
        var angularDateFilter = $filter('date');
        return function (theDate) {
            return angularDateFilter(theDate, 'dd/MM/yyyy'); // Format : 30/11/2013
        };
    }

    function boBoolean() {
        // Directive that returns a BO custom display for the given boolean
        // Ex: <bo-boolean value="myBooleanValue"></bo-boolean>
        return {
            restrict: 'E',
            replace: true,
            scope: {
                boolean: '=value'
            },
            template: '<span class="bo-boolean glyphicon" ng-class="boolean ? \'glyphicon-ok\' : \'glyphicon-remove\'" title="{{boolean}}"></span>'
        };
    }
})();
