﻿

angular.module('jarapp')
    .controller('priceListUploadCtrl', ['$rootScope', '$scope','$state', '$filter', 'repository', 'alertService', '$sessionStorage', 'appSettings', function ($rootScope, $scope,$state, $filter, repository, alertService,  $sessionStorage, appSettings) {

            $scope.postForm = "";
            $scope.dataPost = "";
            $scope.uploader = {};
            $scope.uploadProgress = 0;
            $scope.buyers = null;
            $scope.loggedIn = $sessionStorage.loggedIn;

            $scope.selectedPriceListType = {
                priceListType: null
            };

            $scope.selectedBuyer = {
                buyer: null
            };

            $scope.setSelectedBuyer = function (buyer) {
                $scope.selectedBuyer.buyer = buyer;
            };

            $scope.setSelectedPriceListType = function (priceListType) {
                $scope.selectedPriceListType.priceListType = priceListType;
            };
             
            var unbind = $rootScope.$on('NotifyPartialUploadDataPartialViewEvent', function () {
                $scope.loggedIn = $sessionStorage.loggedIn;
            });
            $scope.$on('$destroy', unbind);

            //INIT BUYERS
            $scope.init = function () {
                repository.getbuyers()
                    .then(function (response) {
                        $scope.buyers = response.data;
                        $scope.selectedBuyer.buyer = $scope.buyers[0];
                    })
                    .catch(function (response) {
                        alertService.add('error', 'Error while getting Buyers    Error: ' + response.statusText);
                    }); 
            };

            //Pass FormData to  flowrequest parsed in backend and saved with file into DB
            $scope.flowConfig = {
                query: function (flowFile, flowChunk) {
                    return {
                        flowAngularControllerId: "uploadDataPartial",
                        flowBuyerId: $scope.dataPost.buyer.BuyerId,
                        flowSubject: $scope.dataPost.subject,
                        flowStartDate: $scope.dataPost.startDate,
                        flowEndDate: $scope.dataPost.endDate,
                        flowTypeDataId: $scope.dataPost.dataType.Id,
                        flowTypeListId: $scope.dataPost.priceListType.Id
                    };
                }
            };

            //UPDATE FILE INFO -- OBSOLETE - NOT DELETE FOR NOW
            $scope.fileSuccess = function (file, id) { 
                 
                var formData = $scope.dataPost;
                var uploadData = { buyerId: formData.buyer.BuyerId, id: id, size: file.size, fileName: file.name, subject: formData.subject, startDate: formData.startDate, endDate: formData.endDate, typeDataId: formData.dataType.Id, typeListId: formData.priceListType.Id };

                repository.uploadData(uploadData).then(function (response) {
                    alertService.add('success', 'Info Updated: ' + response.statusText);
                }).catch(function (response) {
                    alertService.add('error', 'Error while Sending File. Error: ' + response.statusText);
                    });

                $state.go('home.dataPort');

            };

            //SAVE SUBMITED FORM AND FILE
            $scope.saveDataUploader = function (form, dataPost) { 
                //Extend dataPost form when buyer property not exists, only first load when user not selects buyer from dropdown -> better solution needed 

                if (!dataPost.hasOwnProperty('buyer')) {
                    angular.extend(dataPost, {
                        buyer: $scope.selectedBuyer.buyer
                    });
                }

                if (!dataPost.hasOwnProperty('dataType')) {
                    angular.extend(dataPost, {
                        dataType: $scope.selectedDataType.dataType
                    });
                }

                if (!dataPost.hasOwnProperty('priceListType')) {
                    angular.extend(dataPost, {
                        priceListType: $scope.selectedPriceListType.priceListType
                    });
                } 
                $scope.postForm = form;
                $scope.dataPost = dataPost;
                $scope.uploader.flow.opts.target = appSettings.serverPath + appSettings.flowPath;
                $scope.uploader.flow.upload(); 
            }; 

            //GET PRICE LIST TYPES
            repository.getpricelisttypes()
                .then(function (response) {
                    $scope.priceListTypes = response.data;

                    $scope.selectedPriceListType = {
                        priceListType: $scope.priceListTypes[2]
                    };
                })
                  .catch(function (response) {
                      alertService.add('error', 'Error while getting pricelist types  Error: ' + response.statusText);
                  });

            //GET DATA TYPES
            repository.getdatatypes()
             .then(function (response) {
                 $scope.dataTypes = response.data; 

                 $scope.selectedDataType = {
                     dataType: $scope.dataTypes[2]
                 };
             }).catch(function (response) {
                 alertService.add('error', 'Error while getting Data types  Error: ' + response.statusText);
             });

            //Error check
            $scope.dataUploaderFieldHasError = function (dataUploaderForm) {
                if (!dataUploaderForm) {
                    return false;
                }
                return dataUploaderForm.$invalid && dataUploaderForm.$dirty;
            };
        }]);