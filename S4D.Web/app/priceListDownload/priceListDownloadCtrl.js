﻿

angular.module('jarapp')
    .controller('priceListDownloadCtrl', [
        '$rootScope', '$cookies', '$state', 'ngTableParams', '$filter', 'alertService', 'translateService', 'priceListService', 'repository', function ($rootScope,$cookies, $state, ngTableParams, $filter, alertService, translateService, priceListService, repository) {

            var downloadPriceListPartial = this;

            var unbind = $rootScope.$on('NotifyDownloadPriceListPartial', function () {
                downloadPriceListPartial.tableParams.reload();
            });
            downloadPriceListPartial.priceListStatuses = [
                 { id: "", title: "" }, { id: "Draft", title: "Draft" }, { id: "Approved", title: "Approved" }, { id: "Declined", title: "Declined" }, { id: "Update", title: "Update" }
            ];

            downloadPriceListPartial.selectRow = function (priceList) {
                 priceListService.addOnePriceList(priceList); 
                 $cookies.remove("priceListId");
                 $cookies.put("priceListId", priceList.Id);
                 $state.go('home.dataPortDetails');
            }; 

            downloadPriceListPartial.tableParams = new ngTableParams({
                page: 1,
                count: 1000

            }, {
                counts: [],
                getData: function ($defer, params) {
                    repository.getPriceLists  

                    repository.getPriceLists().then(function (htmlData) {
                        var filteredData = params.filter() ?
                                  $filter('filter')(htmlData.data, params.filter()) :
                                  htmlData.data;

                        var orderedData = params.sorting() ?
                            $filter('orderBy')(filteredData, params.orderBy()) :
                            filteredData;
                        params.total(htmlData.length);

                        var resolve = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        $defer.resolve(orderedData.slice(resolve));

                    }).catch(function (response) {
                        alertService.add('error', 'Error while retrieving Price Lists. Error: ' + response);
                    });


                    //repository.getPriceLists().then(function (htmlData) { 

                    //    var filteredData = params.filter() ?
                    //              $filter('filter')(htmlData.data, params.filter()) :
                    //              htmlData.data;

                    //    var orderedData = params.sorting() ?
                    //    $filter('orderBy')(filteredData, params.orderBy()) :
                    //    filteredData;
                    //    params.total(orderedData.length);

                    //    var resolve = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    //    $defer.resolve(orderedData.slice(resolve)); 

                    //    }).catch(function (response) {
                    //        alertService.add('error', 'Error while retrieving Price Lists. Error: ' + response);
                    //    }); 
                }, downloadPriceListPartial: { $data: downloadPriceListPartial.data }
            });
            downloadPriceListPartial.tableParams.settings().downloadPriceListPartial = downloadPriceListPartial; //Fixes $emit error

        }]);