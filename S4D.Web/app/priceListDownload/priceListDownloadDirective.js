﻿angular.module('jarapp').directive(
    'priceListDownloadDirective',
    function () {
        return ({
            scope: {
                name: '='
            },
            controller: "priceListDownloadCtrl",
            controllerAs: "downloadPriceListPartial",
            link: link,
            restrict: "A",
            bindToController: true,
            templateUrl: 'app/priceListDownload/priceListDownload.html'
        });

        function link(scope, element, attributes) {
        }

    }
);