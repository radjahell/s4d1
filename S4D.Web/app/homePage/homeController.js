﻿

angular.module('jarapp')
    .controller('HomeController', [
          '$cookies', '$rootScope', '$scope', 'alertService', '$state', '$sessionStorage', function ( $cookies, $rootScope, $scope, alertService, $state, $sessionStorage) {
          
            $scope.showPreview = true;
            $scope.loggedInUser = $sessionStorage.loggedInUserName;
            $scope.loggedInUserWelcome = $cookies.get("loggedInUser");
            $sessionStorage.loggedInUserName = ""; 
            $scope.loggedInUserName = '';
            $scope.loggedIn = $sessionStorage.loggedIn;

            $scope.goToDownloads = function () { 
                $state.go('home.dataPort');
            }; 

            $scope.loginFieldHasError = function (loginForm) {
                if (!loginForm) {
                    return false;
                }
                return loginForm.$invalid && loginForm.$dirty;
            };

            //Handle login
            $scope.login = function (loginForm, login) {
                if (login.inputEmail) {
                    $cookies.remove("loggedInUser");
                    $cookies.put("loggedInUser", login.inputEmail);

                    $scope.loggedInUserWelcome = $cookies.get("loggedInUser");

                    $scope.loggedInUser = login.inputEmail;    //$sessionStorage.loggedInUserName;
                    $sessionStorage.loggedIn = true;
                } else {
                    $sessionStorage.loggedIn = false;
                } 
                $rootScope.$emit('NotifyPartialUploadDataPartialViewEvent');
                $scope.loggedIn = $sessionStorage.loggedIn;
                $scope.loggedInUserName = login.inputEmail;

                $rootScope.$emit('NotifyHeaderLoggedInUser', login.inputEmail);
                $sessionStorage.loggedInUserName = login.inputEmail;
            }

        }]);