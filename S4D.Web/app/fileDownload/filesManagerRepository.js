﻿ 
angular.module('jarapp')
    .service('filesManagerRepository', [
        '$http', 'appSettings', '$q', function ($http, appSettings, $q) {
            var vm = this;
            
            vm.getFiles = function () {
                var files = $http.get(appSettings.serverPath + '/api/files');
                return files;
            };

            vm.getFileById = function (id) {
                var  files = $http.get(appSettings.serverPath + '/api/file/' + id );
                return files; 
            }; 

            vm.downloadFile = function (id) {
                console.log("DOWNLOAD FILE");
            }; 

            vm.sendFile = function (formData) {
                return $http.post(appSettings.serverPath + '/api/file', formData, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                });
            };

            vm.deleteFile = function (id) {
                var url = appSettings.serverPath + '/api/delete/' + id;  
                return $http.post(url).then(function (response) { 
                     return response;
                }).catch(function (response) {
                    return response;
                }); 
            };

        }]);
