﻿
angular.module('jarapp')
.controller('fileDownloadCtrl', ['$scope', 'filesManagerRepository', '$state', 'ngTableParams', 'alertService', 'ModalService', function ($scope, filesManagerRepository, $state, ngTableParams, alertService, ModalService) {

   var vm = this;
   vm.title = 'Data Archive';
   vm.temporaryId = 0; 
   vm.files = [];
   vm.fileId = '';
   vm.downloadedFile = '';

   if ($scope.$parent.downloadFile != null) {
       vm.fileId = $scope.$parent.downloadFile.fileId;
   } 
        //Shows DeleteFileModal popup
        vm.showModal = function (id) {
            vm.temporaryId = id;
            ModalService.showModal({
                templateUrl: 'app/modals/deleteFileModal.html',
                controller: "ModalController"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    //If User clicks yes in the modal it triggers delete method with the file Id
                    if (result === true) {
                        vm.deleteFile(vm.temporaryId);
                    }
                });
            });
        };

        if (vm.fileId != "") {
            filesManagerRepository.getFileById(vm.fileId)
             .then(function (files) {
                 vm.files = files.data;
             }).catch(function (response) {
                 alertService.add('error', 'Error while retreving files. Error: ' + response.statusText);
             });
        }
        else {
            filesManagerRepository.getFiles()
              .then(function (files) {
                  vm.files = files.data;
              }).catch(function (response) {
                  alertService.add('error', 'Error while retreving files. Error: ' + response.statusText);
              });
        };

        vm.downloadFile = function (id) {
            filesManagerRepository.downloadFile(id)
             .then(function (file) {
                 vm.downloadedFile = file.data;
             }).catch(function (response) {
                 alertService.add('error', 'Error while downloading file. Error: ' + response.statusText);
             });
        };

        //Delete file by fileId
        vm.deleteFile = function (id) {
            filesManagerRepository.deleteFile(id).then(function (response) {
                //Handle ID of delete file in data
                $state.reload();
                
            }).catch(function (response) {
                alertService.add('error', 'Error while deleting file. Error: ' + response.statusText);
            });;
        };
    }
 ])  ;