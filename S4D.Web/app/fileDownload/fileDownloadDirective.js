﻿angular.module('jarapp').directive(
    'fileDownloadDirective',
    function () {
        return ({
            scope: {
                name: '='
            },
            controller: "fileDownloadCtrl",
             controllerAs: "vm", 
            link: link,
            restrict: "A",
            bindToController: true,
            templateUrl: 'app/fileDownload/fileDownload.html'
        });

        function link(scope, element, attributes) { 
        }

    }
);

