﻿'use strict';

angular
    .module('TestServices', [])
    .factory('supplierService', supplierService);

supplierService.$inject = ['$rootScope'];

function supplierService($rootScope) {

    // create an array of alerts available globally
    $rootScope.suppliers = [
    {
        Name: "KOFOLA",
        Ico: "789456" ,
        Vat:  "654987",
        Street: "Bratislavska" ,
        City: "Kosice" 
    }, {
        Name: "NESTLE",
        Ico: "789456",
        Vat: "654987",
        Street: "Ostravska",
        City: "Brno"
    }];

    var addSupplier = function (supplier) { 
        $rootScope.suppliers.push({
            Name: supplier.name,
            Ico: supplier.ico,
            Vat: supplier.vat,
            Street: supplier.street,
            City: supplier.city
        }); 
    };

    var getAllSuppliers = function() {
        return $rootScope.suppliers;
    };

    var removeSupplier = function (index) {
        $rootScope.suppliers.splice(index, 1);
    };

    var clearSupplier = function () {
        $rootScope.suppliers = [];
    };

    return {
        addSupplier: addSupplier,
        removeSupplier: removeSupplier,
        clearSupplier: clearSupplier,
        getAllSuppliers: getAllSuppliers
    };
}

