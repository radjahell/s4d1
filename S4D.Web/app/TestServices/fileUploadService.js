﻿'use strict';

angular
    .module('TestServices', [])
    .factory('fileUploadService', fileUploadService);

fileUploadService.$inject = ['$rootScope'];

function fileUploadService($rootScope) {

    $rootScope.files = [
    {
        sender: "Jan",
        message: "Check it",
        subject: "File for upload",
        receiver: "Jan Behal"
    }, {
        sender: "Josef",
        message: "Check it more",
        subject: "New File",
        receiver: "Jan Dobeh"
    }];

    var addFile = function (file) {
        $rootScope.files.push({
            sender: file.sender,
            message: file.message,
            subject: file.subject,
            receiver: file.receiver
        });

    };

    var getAllFiles = function () {
        return $rootScope.files;
    };

    var removeFile = function (index) {
        $rootScope.files.splice(index, 1);
    };

    var clearFiles = function () {
        $rootScope.files = [];
    };

    return {
        addFile: addFile,
        removeFile: removeFile,
        clearFiles: clearFiles,
        getAllFiles: getAllFiles
    };
}

