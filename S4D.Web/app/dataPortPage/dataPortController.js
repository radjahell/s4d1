﻿'use strict';

angular.module('jarapp')
        .controller('DataPortController', ['$scope', '$rootScope', function ($scope,$rootScope) { 
            var unbind = $rootScope.$on('NotifyDownloadPriceListPartial', function () {
                $scope.tab = 1;
            });

            $scope.$on('$destroy', unbind); 
        }]);