﻿'use strict';

angular.module('jarapp')
        .controller('DownloadFileController', ['$stateParams' , function ($stateParams) {

            var vm = this; 
            vm.fileId = $stateParams.fileId; 
        }]);