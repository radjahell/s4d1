﻿'use strict';

angular.module('jarapp', [
    'ui.router', 'ngResource', 'pascalprecht.translate', 'ngTable', 'Alerts', 'ui.grid', 'TestServices', 'ngCookies', 'ui.bootstrap.datetimepicker', 'boDirectives', 'boDataType', 'ngStorage', 'angularModalService', 'flow'])
    .config(function ($stateProvider, $urlRouterProvider, $translateProvider, $locationProvider, $compileProvider, flowFactoryProvider) {

        flowFactoryProvider.defaults = {
            permanentErrors: [404, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 4
        }; 

        //clean blob of unsafe attr
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|):/);

        $stateProvider
            // route for the home page
            .state('home', {
                url: '/',
                views: {
                    'header': {
                        templateUrl: 'app/headerPage/header.html',
                        controller: 'HeaderController'
                    },
                    'content': {
                        templateUrl: 'app/homePage/home.html',
                        controller: 'HomeController'
                    },
                    'footer': {
                        templateUrl: 'app/footerPage/footer.html',
                        controller: 'FooterController'
                    }
                }
            })
            .state('home.dataPort', {
                url: 'dataport',
                views: {
                    'content@': {
                        templateUrl: 'app/dataPortPage/dataPort.html',
                        controller: 'DataPortController'
                    }
                }
            }) 
            .state('home.admin', {
                url: 'admin',
                views: {
                    'content@': {
                        templateUrl: 'app/adminPage/admin.html',
                        controller: 'AdminController'
                    }
                }
            }).state('home.dataPortDetails', {
                url: 'dataportdetails',
                views: {
                    'content@': {
                        templateUrl: 'app/priceListDownloadDetail/priceListDownloadDetail.html',
                        controller: 'priceListDownloadDetailCtrl',
                        controllerAs: 'downloadPriceListDetails'
                    }
                }

            }).state('home.files', {
                url: 'files',
                views: {
                    'content@': {
                        templateUrl: 'app/fileDownload/fileDownload.html',
                        controller: 'fileDownloadCtrl',
                        controllerAs: 'vm'
                    }
                }
            }).state('home.downloadFile', {
                url: 'downloadFile/{fileId}', 
                views: {
                    'content@': {
                        templateUrl: 'app/downloadFile/downloadFile.html',
                        controller: 'DownloadFileController',
                        controllerAs: 'vm'
                    }
                }
            });

        $urlRouterProvider.otherwise('/');

        //  {{ '' | translate }}  --dopln tag nize a zkopiruj na misto
        $translateProvider.translations('cz', {
            'TITLE': 'Návod',
            'DESCRIPTION1': '1. Klikněte na řádek. ',
            'DESCRIPTION2': '2. Na pravé straně se objeví download.',
            'LoginPanelHeading': 'Přihlášeni',
            'LoginInputEmail': 'Email',
            'LoginInputEmailError': 'Email musí být vyplněn.',
            'LoginInputPassword': 'Heslo',
            'LoginInputPasswordError': 'Heslo musí byt vyplněné',
            'LoginRememberMe': 'Zapamatovat si',
            'LoginLogMeIn': 'Přihlásit se',
            'DataPostReceiver': 'Komu',
            'DataPostReceiverError': 'Komu musí byt vyplněné',
            'DataPostSubject': 'Předmět',
            'DataPostSubjectError': 'Předmět musí být vyplněný',
            'DataPostMessage': 'Zpráva',
            'DataPostMessageError': 'Zpráva musí být vyplněná',
            'DataPostSend': 'Odeslat',
            'DataPostSender': 'Odesílatel',
            'DataPostSenderError': 'Odesílatel musí být vyplněn',
            'DataUploaderListName': 'Název',
            'DataUploaderListNameError': 'Název musí byt vyplněn',
            'DataUploadeDataType': 'Typ datového souboru',
            'DataUploadePriceListType': 'Typ ceníku',
            'DataUploaderStartDate': 'Platné od:',
            'DataUploaderEndDate': 'Platné do:',
            'SendFileForm': 'Pošli soubor',
            'DataUploadForm': 'Nahraj soubor',
            'FileInput': 'Vyberte Soubor',
            'FileInputInfo': 'Žádný soubor nebyl vybrán',
            'UploadData': 'Nahrát soubor',
            'DataUploaderEndDatePlaceholder': 'Vyberte platnost do',
            'DataUploaderStartDatePlaceholder': 'Vyberte platnost od',
            'LoginWelcome': 'Vítejte',
            'UserNameWelcome': 'Uživatel',
            'SendPriceListForm': 'Pošli ceník emailem',
            'ReceiverName': 'Příjemce',
            'Home': 'Domů',
            'Admin': 'Administrace',
            'DataPortOverview': 'Přehled nahraných souborů',
            'Files': 'Soubory',
            'Approve': 'Schválit',
            'Decline': 'Zamítnout',
            'Update': 'Aktualizovat',
            'Cancel': 'Storno',
            'Logout': 'Odhlášení',
            'PRICELIST': 'Cenik',
            'DataArchive': 'Archív Data'

        });


        $translateProvider.translations('en', {
            'TITLE': 'Manual',
            'DESCRIPTION1': '1. Click the row. ',
            'DESCRIPTION2': '2. On the right side will appear the download form. ',
            'LoginPanelHeading': 'Login',
            'LoginInputEmail': 'Email',
            'LoginInputEmailError': 'Email is required',
            'LoginInputPassword': 'Password',
            'LoginInputPasswordError': 'Password is required.',
            'LoginRememberMe': 'Remember me',
            'LoginLogMeIn': 'Login in',
            'DataPostReceiver': 'To',
            'DataPostReceiverError': 'To is required',
            'DataPostSubject': 'Subject',
            'DataPostSubjectError': 'Subject is required',
            'DataPostMessage': 'Message',
            'DataPostMessageError': 'Message is required',
            'DataPostSend': 'Send',
            'DataPostSender': 'Sender',
            'DataPostSenderError': 'Sender is required',
            'DataUploaderListName': 'Title',
            'DataUploaderListNameError': 'Title is required',
            'DataUploadeDataType': 'Data Type',
            'DataUploadePriceListType': 'Price List Type',
            'DataUploaderStartDate': 'Valid from:',
            'DataUploaderEndDate': 'Valid until:',
            'SendFileForm': 'Send File',
            'DataUploadForm': 'Upload Data',
            'FileInput': 'Choose File',
            'FileInputInfo': 'No file choosen',
            'UploadData': 'Upload Data',
            'DataUploaderEndDatePlaceholder': 'Please select an end date',
            'DataUploaderStartDatePlaceholder': 'Please select a start date',
            'LoginWelcome': 'Welcome',
            'UserNameWelcome': 'User',
            'SendPriceListForm': 'Send Price List by email',
            'ReceiverName': 'Reciever',
            'Home': 'Home',
            'Admin': 'Administration',
            'DataPortOverview': 'Data Port Overview',
            'Logout': 'Logout',
            'PRICELIST': 'Price List',
            'DataArchive': 'Data Archive'
        });

        $translateProvider.preferredLanguage('en');

    })
    .constant("appSettings", {
        serverPath: "http://localhost:49738",
        flowPath: "/folders/uploads/uploadsFolder"
    })
    .run(function ($rootScope, alertService) {
        $rootScope.closeAlert = alertService.remove; 
    });
 
    
 
 

