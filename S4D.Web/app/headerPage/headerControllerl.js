﻿'use strict';

angular.module('jarapp')
        .controller('HeaderController', ['$rootScope', '$cookies', '$state', 'translateService', 'alertService', '$sessionStorage', function ($rootScope,$cookies, $state, translateService, alertService, $sessionStorage) {

        var header = this;
        header.loggedInUserName = $cookies.get("loggedInUser");
        header.isUserLoggedIn = $sessionStorage.loggedIn;

        $rootScope.$on('NotifyHeaderLoggedInUser', function () {
            header.loggedInUserName = $cookies.get("loggedInUser");
        });
      
        header.isVisible = function () {
                return $state.current.name !== "home"; 
            } 

        header.changeLanguage = function (langKey) {
            translateService.changeLanguage(langKey);
            alertService.add('success', 'Language changed to: ' + langKey);
        };

        header.logOut = function () {
            $sessionStorage.loggedIn = false;
            $cookies.remove("loggedInUser");
            $state.go('home');
        };

    }]);