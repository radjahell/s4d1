﻿
angular.module('jarapp')
        .controller('priceListDownloadDetailCtrl', ['$scope', '$cookies', '$state', 'repository', 'alertService', function ($scope, $cookies, $state, repository, alertService) {

            var downloadPriceListDetails = this;
            downloadPriceListDetails.priceList = "";
            downloadPriceListDetails.url = '';
            downloadPriceListDetails.downloadName = '';
            downloadPriceListDetails.priceListId = $cookies.get("priceListId");
            downloadPriceListDetails.gridOptions = {};

            //CREATE BLOB FROM FILE IN DB - STRING -> BLOB
            function createExcelBlob(stringDecodedFile) {
                var blob = '';

                try {
                    var byteCharacters = atob(stringDecodedFile.File);
                    var byteNumbers = new Array(byteCharacters.length);
                    for (var i = 0; i < byteCharacters.length; i++) {
                        byteNumbers[i] = byteCharacters.charCodeAt(i);
                    }
                    var byteArray = new Uint8Array(byteNumbers);

                    blob = new Blob([byteArray], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    return blob;
                } catch (e) {
                    alertService.add('error', 'Error converting file to blob: ' + e);
                }
                return blob;
            };


            //CREATE EXCEL OBJECT AND SHOW IT IN VIEW
            function showExcel(blob) {

                try {
                    var reader = new FileReader();

                    reader.onload = function (excel) {
                        $scope.$apply(function () {

                            var result = excel.target.result;

                            var workbook = XLSX.read(result, { type: 'binary' });
                            var headerNames = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], { header: 1 })[0];
                            var data = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);

                            downloadPriceListDetails.gridOptions.columnDefs = [];
                            headerNames.forEach(function (h) {
                                downloadPriceListDetails.gridOptions.columnDefs.push({ field: h });
                            });

                            downloadPriceListDetails.gridOptions.data = data;
                        });
                    };

                    reader.readAsBinaryString(blob);
                } catch (e) {
                    alertService.add('error', 'Error converting excel object to be shown in view: ' + e);
                }
            };

            //INITIALIZE DATA -> DETAILS AND EXCEL GRID
            downloadPriceListDetails.init = function () {

                repository.getPriceListById(downloadPriceListDetails.priceListId)
                     .then(function (response) {

                         downloadPriceListDetails.priceList = response.data;

                         var blob = createExcelBlob(downloadPriceListDetails.priceList);
                         showExcel(blob);

                         var objectUrl = URL.createObjectURL(blob);

                         downloadPriceListDetails.url = objectUrl;
                         downloadPriceListDetails.downloadName = downloadPriceListDetails.priceList.FileName;

                     })
                     .catch(function (response) {
                         alertService.add('error', 'Error while getting getPriceListById    Error: ' + response.statusText);
                     });
            }

            downloadPriceListDetails.setPriceListStatusToApproved = function (id) {
                repository.setPriceListStatusToApproved(id).then(function (response) {

                    downloadPriceListDetails.init();
                })
                    .catch(function (response) {
                        alertService.add('error', 'Error updating priceList to Approve: ' + response.statusText);
                    });
            };

            downloadPriceListDetails.setPriceListStatusToDeclined = function (id) {
                repository.setPriceListStatusToDeclined(id).then(function (response) {
                    downloadPriceListDetails.init();
                })
                       .catch(function (response) {
                           alertService.add('error', 'Error updating priceList to Declined: ' + response.statusText);
                       });
            };

            downloadPriceListDetails.setPriceListStatusToUpdated = function (id) {
                repository.setPriceListStatusToUpdated(id).then(function (response) {
                    downloadPriceListDetails.init();
                })
                       .catch(function (response) {
                           alertService.add('error', 'Error updating priceList to Updated: ' + response.statusText);
                       });
            };

            downloadPriceListDetails.goHome = function () {
                $state.go('home.dataPort');
            };
        }]);