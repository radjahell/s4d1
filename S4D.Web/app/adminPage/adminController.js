﻿'use strict';

angular.module('jarapp') 
         .controller('AdminController', ['$scope', 'ngTableParams', '$filter', 'repository', 'alertService', 'translateService', 'supplierService', function ($scope, ngTableParams, $filter, repository, alertService, translateService, supplierService) {

      
        $scope.save = function(supplierForm, supplier) { 
            supplierService.addSupplier(supplier); 
            $scope.tableParams.reload();
        };

        $scope.tableParams = new ngTableParams({
                 page: 1,
                 count: 1000
                 }, {
                     counts: [],
                     getData: function ($defer, params) {
                         var htmlData = supplierService.getAllSuppliers(); 

                         var resolve = htmlData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                         $defer.resolve(htmlData.slice(resolve));
                     }, $scope: { $data: $scope.data }
                 });
        $scope.tableParams.settings().$scope = $scope; //Fixes $emit error

        //$scope.tableParams = new ngTableParams({
        //        page: 1,
        //        count: 1000

        //    }, {
        //        counts: [],
        //        getData: function ($defer, params) {

        //            repository.getSuppliers().then(function (htmlData) {

        //                var filteredData = params.filter() ?
        //                          $filter('filter')(htmlData.data, params.filter()) :
        //                          htmlData.data;

        //                var orderedData = params.sorting() ?
        //                $filter('orderBy')(filteredData, params.orderBy()) :
        //                filteredData;
        //                params.total(orderedData.length);

        //                var resolve = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
        //                $defer.resolve(orderedData.slice(resolve));

        //            }).catch(function (response) {
        //                console.log('Error while retrieving API data.');
        //            });
        //        }, $scope: { $data: $scope.data }
        //    });

        }]);