﻿angular.module('jarapp')
.controller('ModalController', function ($scope, close) {

    //Controller for Generic popups 
    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };

});