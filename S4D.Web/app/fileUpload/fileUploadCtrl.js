﻿

angular.module('jarapp')
    .controller('fileUploadCtrl', ['priceListService', '$scope', '$state', 'typesService', 'filesManagerRepository', 'alertService', 'repository', 'appSettings', function (priceListService, $scope, $state, typesService, filesManagerRepository, alertService, repository, appSettings) {

        $scope.postForm = "";
        $scope.dataPost = "";
        $scope.fileuploader = {};
        $scope.uploadProgress = 0;
        $scope.receivers = typesService.getReceivers(); 

        $scope.selectedReceiver = {
            receiver: $scope.receivers[3]
        };

        $scope.buyers = null;

        $scope.selectedBuyer = {
            buyer: null
        };

        $scope.setSelectedBuyer = function (buyer) {
            $scope.selectedBuyer.buyer = buyer;
        }

        //INIT BUYERS -> Need to be fixed
        $scope.init = function () {

            repository.getbuyers()
                .then(function (response) {
                    $scope.buyers = response.data;
                    $scope.selectedBuyer.buyer = $scope.buyers[0];
                })
                .catch(function (response) {
                    alertService.add('error', 'Error while getting Buyers    Error: ' + response.statusText);
                });

        }

        //UPDATE FILE INFO
        $scope.fileUploadSuccess = function (file, id ) { 

            var formData = $scope.dataPost;
            var sentFile = { buyerId: formData.buyer.BuyerId, id: id, fileName: file.name, message: formData.message, size: file.size, subject: formData.subject };
            repository.sendFile(sentFile).then(function (response) {
                alertService.add('success', 'Info Updated: ' + response.statusText);
            }).catch(function (response) {
                alertService.add('error', 'Error while Sending File. Error: ' + response.statusText);
            });

            $state.go('home.files');

        };

        $scope.dataPostFieldHasError = function (dataPostForm) {
            if (!dataPostForm) {
                return false;
            }
            return dataPostForm.$invalid && dataPostForm.$dirty;
        };

        //Pass id of the controller to flowrequest 
        $scope.flowConfig = {
            query: function (flowFile, flowChunk) {
                return {
                    flowAngularControllerId: "sendFilePartial"
                };
            }
        }; 

        $scope.saveDataPost = function (form, dataPost) {

            //Extend dataPost form when buyer property not exists, only first load when user not selects buyer from dropdown -> better solution needed 
            if (!dataPost.hasOwnProperty('buyer')) {

                angular.extend(dataPost, {
                    buyer: $scope.selectedBuyer.buyer
                });
            }

            $scope.postForm = form;
            $scope.dataPost = dataPost;

            $scope.fileuploader.flow.opts.target = appSettings.serverPath + appSettings.flowPath;
            $scope.fileuploader.flow.upload();

        }
    }]);